<?php

return [
    'state' => [
        'New South Wale' => 'New South Wale',
        'Queensland' => 'Queensland',
        'Far North Queensland' => 'Far North Queensland',
        'South Australia' => 'South Australia',
        'Tasmania' => 'Tasmania', 
        'Victoria' => 'Victoria', 
        'Western Australia' => 'Western Australia', 
        'Other State' => 'Other State',
    ]
];

?>