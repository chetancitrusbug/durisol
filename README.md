# My project's README

// Added Slug field on Banner Category Table
ALTER TABLE `bannercategory` ADD `slug` VARCHAR(191) NOT NULL AFTER `title`;

// Added Slug field on Blog Table
ALTER TABLE `blogs` ADD `slug` VARCHAR(191) NOT NULL AFTER `title`;

// Added Slug field on Blog Category Table
ALTER TABLE `blog_category` ADD `slug` VARCHAR(191) NOT NULL AFTER `title`;

// Before migration
path:- C:\xampp\htdocs\laravel\durisol\app\Providers\AppServiceProvider.php
 -- comment two lines
 
		 self::$_portfolio_category = Bannercategory::where('status','active')->get();
         self::$_setting = Setting::where('id','1')->get();
		 
//After MIgration

 -- add default entry in settings table
