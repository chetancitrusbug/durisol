<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::group(['prefix' => 'v1.0/', 'middleware' => 'api_pass', 'namespace' => 'Api'], function () {

   Route::put('user/changepassword', 'UsersController@changePassword');
   Route::put('user/update', 'UsersController@update');
   Route::post('user/forgotpassword', 'UsersController@forgotPasswordPost');
   Route::get('user/logout', 'UsersController@logout');
   Route::post('user/getopportunity', 'UsersController@getopportunity');
   
});

Route::group(['prefix' => 'v1.0/',  'namespace' => 'Api'], function () {
    Route::post('user/login', 'UsersController@login');
    Route::post('user/register', 'UsersController@register');
    Route::post('user/forgotpassword', 'UsersController@forgotPasswordPost');
    Route::post('user/getpackage', 'UsersController@getpackage');

});