<?php


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@aboutindex');
Route::get('/pricing', 'HomeController@pricingindex');
Route::get('/privacy-policy', 'HomeController@privacyindex');
Route::get('/technical-info', 'HomeController@technicalindex');
Route::get('/testimonials', 'HomeController@testimonialindex');
Route::get('/free-download', 'FreeimagedownloaduserlogController@index');
Route::get('/free-download/checkvalid', 'FreeimagedownloaduserlogController@checkvalid');
Route::get('/freedownloaduser', 'FreeimagedownloaduserlogController@add');
Route::post('/freedownloaduser', 'FreeimagedownloaduserlogController@add');
Route::group(['prefix' => 'blog'], function() {
    // blog    
    Route::get('/', 'BlogController@index');
    Route::get('/view/{slug}', 'BlogController@view');
    Route::get('/category/{category}', 'BlogController@serchbycategory');
});

Route::group(['prefix' => 'trainings'], function() {
    // blog    
    Route::get('/', 'TrainingController@index');
    Route::get('/view/{slug}', 'TrainingController@view');
    Route::get('/category/{category}', 'TrainingController@serchbycategory');
});

Route::group(['prefix' => 'portfolio'], function() {
    //Portfolio
    Route::get('/view/{slug}', 'PortfolioController@view');
});

//Contact
Route::resource('/contact', 'ContactController');




    Route::group(['middleware' => ['auth', 'admin']], function () {
    
    Route::get('/admin', 'HomeController@redirect');

    Route::group(['prefix' => 'admin'], function () {

         //generator
         Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
         Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

        Route::get('/admin', 'Admin\AdminController@index');
        Route::resource('/users', 'Admin\UsersController');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/permissions', 'Admin\PermissionsController');
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');

        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');

         
        //Blog 
        Route::resource('/blogs', 'Admin\BlogController');
        Route::get('/blogData', ['as' => 'blogData',
         'uses' => 'Admin\BlogController@datatable']);
         Route::post('/blogs/deleteimage', 'Admin\BlogController@deleteimage');
         Route::post('/blogs/deleteimage', 'Admin\BlogController@deleteimage');
    
        // Blog Category
        Route::resource('/blogcategory', 'Admin\BlogcategoryController');
        Route::get('/blogCategoryData', ['as' => 'blogCategoryData',
         'uses' => 'Admin\BlogcategoryController@datatable']);

        // Banner Category
        Route::resource('/bannerCategory', 'Admin\BannercategoryController');
        Route::get('/bannerCategoryData', ['as' => 'bannerCategoryData',
         'uses' => 'Admin\BannercategoryController@datatable']);
        //  Route::get('/portfoliocatsdata', ['as' => 'portfoliocatlistData', 'uses' => 'Admin\BannercategoryController@portfoliocatlistdatatable']);
        // Route::get('/bannerCategory/{id}/portfoliocatlist', 'Admin\BannercategoryController@portfiliocatlistindex');
        
        // Banner 
        Route::resource('/banner', 'Admin\BannerController');
        Route::get('/bannerData', ['as' => 'bannerData',
         'uses' => 'Admin\BannerController@datatable']);

           // Free Banner 
        Route::resource('/freebanner', 'Admin\FreebannerController');
        Route::get('/freebannerData', ['as' => 'freebannerData','uses' => 'Admin\FreebannerController@datatable']);


         // Freeimage Userlog 
        Route::resource('/imageuserlog', 'Admin\FreeimagedownloaduserlogController');
        Route::get('/imageuserlogData', ['as' => 'imageuserlogData','uses' => 'Admin\FreeimagedownloaduserlogController@datatable']);

   
        
         // Bulk Upload 
        Route::get('/bulkupload', 'Admin\PortfolioController@bulkcreate');
        Route::post('/bulkupload', 'Admin\PortfolioController@bulkstore');

        
        // contact us
        Route::resource('/contact', 'Admin\ContactController');
        Route::get('/contactData', ['as' => 'contactData',
         'uses' => 'Admin\ContactController@datatable']);


         //Training
        Route::resource('/training', 'Admin\TrainingController');
        Route::get('/trainingData', ['as' => 'trainingData', 'uses' => 'Admin\TrainingController@datatable']);
        Route::post('/training/deleteimage', 'Admin\TrainingController@deleteimage');
    
        // Training Category
        Route::resource('/trainingcategory', 'Admin\TrainingcategoryController');
        Route::get('/trainingCategoryData', ['as' => 'trainingCategoryData', 'uses' => 'Admin\TrainingcategoryController@datatable']);

        //Training
         Route::resource('/testimonial', 'Admin\\TestimonialController');

        //Events        
         Route::resource('/events', 'Admin\\EventsController');

          // Setting
        Route::resource('/setting', 'Admin\SettingController');

    });

});
Route::get('/home', 'HomeController@redirect');







