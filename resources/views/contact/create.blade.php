@extends('layouts.master2')
@section('title','Contact Us')

@section('content')

{{--  <div class="page-header contact padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <h1>Contact us</h1>
                            <h3><span class="heading-shape">Contact <strong>us</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                     <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">Contact us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================Contact start==============//-->
        <div class="padB100 padT100">
            <div class="container">
                <!-- Theme Heading -->
                <div class="theme-heading">
                    <h1>Contact us</h1>
                    <h3><span class="heading-shape">Contact <strong>us</strong></span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <!-- Theme Heading -->
            </div>
             @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if (Session::has('flash_error'))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_error') }}
                        </div>
                    @endif
                    @if (Session::has('flash_success'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                    @include('flash::message')
            <div class="contact-us padT100 padB100">
                <div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="address">
								<h3>Contact</h3>
								<ul>
									<li><span>Address:</span>{{$setting->address}}</li>
									<li><span>Contact no:</span>{{$setting->contactno}}</li>
									<li><span>Email:</span>{{$setting->email}}</li>
								</ul>
							</div>
							
						</div>

                    <div class="col-md-8 col-sm-6 col-xs-12">
						 {!! Form::open(['url' => '/contact', 'class' => 'theme-form','id'=>'formContact','enctype'=>'multipart/form-data']) !!}
                             @include ('contact.form')
                         {!! Form::close() !!}
                    </div>
					</div>
                </div>
            </div>
			<div class="contact-map">
				<div class="container">						
					<div class="map-area">
					   <!--<div id="gmap_canvas" class="maps open"></div>-->
                       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3673.599814479989!2d76.043710415513!3d22.96496078498227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3963179b9e77a2a7%3A0x885ddeaea6ed6c72!2s12%2C+Civil+Lines+Rd%2C+Sadashiv+Nagar%2C+Pachunkar+Colony%2C+Itawa%2C+Dewas%2C+Madhya+Pradesh+455001!5e0!3m2!1sen!2sin!4v1521798540615" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
            <div class="clear"></div>
        </div>
        <!--//================Contact end==============//-->
@endsection  --}}

<section class="breadcrumb-wrapper breadcrumb-dark">
    <div class="shell">
      <ol class="breadcrumb-custom">
        <li><a href="{{url('/')}}">Home</a></li>
        <li>Contacts
        </li>
      </ol>
    </div>
  </section>
  <!-- RD Google Map-->
  @foreach($_setting as $settings)
  <section class="section bg-secondary-2 context-dark box-contact"> 
    <div class="range range-condensed range-middle">
      <div class="cell-sm-6">
        <div class="box-content section-md text-left">
          <div class="shell">
            <h3>Contacts</h3>
           
            <div class="range range-30">
              <div class="cell-sm-10">
                <h5>Call us</h5>
                <ul>
                  <li>@if($settings->contactno)<a href="tel:{{$settings->contactno}}">{{$settings->contactno}}</a>@endif</li>
                  <li>@if($settings->email)<a href="mailto:{{$settings->email}}">{{$settings->email}}</a>@endif</li>
                  <li>@if($settings->address){{$settings->address}}@endif</li>
                </ul>
              </div>
              <div class="cell-sm-10">
                <h5>FOLLOW</h5>
                <ul>
                  <li>@if($settings->instagram)<a href="#">{{$settings->instagram}}</a>@endif</li> 
                  <li>@if($settings->facebook)<a href="#">{{$settings->facebook}}</a>@endif</li>
                  <li>@if($settings->twitter)<a href="#">{{$settings->twitter}}</a>@endif</li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="cell-sm-6"> 
        <div class="rd-google-map rd-google-map__model" data-zoom="13" data-y="-32.064205862719824" data-x="115.90368738782195" data-styles="[{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#444444&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;labels.icon&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#46bcec&quot;},{&quot;visibility&quot;:&quot;on&quot;}]}]">
          <ul class="map_locations">
            <li data-y="-32.064205862719824" data-x="115.90368738782195">
              @if($settings->address)
                <p>{{$settings->address}}</p>
              @endif
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  @endforeach
  
  <section class="section bg-primary section-lg context-dark">
    <div class="shell">
      <h3>Durisol States Offices</h3>
      <div class="range">
        <div class="cell-xs-12">
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-carousel-light-dots owl-carousel-type-1" data-items="1" data-sm-items="2" data-md-items="4" data-dots="true" data-nav="true" data-stage-padding="15" data-loop="true" data-margin="30" data-mouse-drag="true" data-autoplay="true" data-autoplay-timeout="1000">
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-new-south-wales.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Andrew McLeod</h5>
                  <p class="small">New South Wales</p>
                </div>
      <p class="text"><a class="button button-circle button-steel-blue" href="mailto:amcleod@durisol.com.au?subject=Durisol enquiry from website">amcleod@durisol.com.au</a><br>Tel: 02 9868 4207<br>Mobile: 0416 138 943</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-queensland.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Philip Craig</h5>
                  <p class="small">Queensland</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:pcraig@durisol.com.au?subject=Durisol enquiry from website">pcraig@durisol.com.au</a><br>Tel. 07 5430 2221<br>Mobile: 0468 357 882</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-queensland-north.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Uwe Jacobs</h5>
                  <p class="small">Far North Queensland</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:ujacobs@durisol.com.au?subject=Durisol enquiry from website">ujacobs@durisol.com.au</a><br>Tel. 03 9758 5331<br>Mobile: 0400 417 043</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-south-australia.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>John Wauchope</h5>
                  <p class="small">South Australia</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:jwauchope@durisol.com.au?subject=Durisol enquiry from website">jwauchope@durisol.com.au</a><br>Mobile: 0430 302 680</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-tasmania.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Uwe Jacobs</h5>
                  <p class="small">Tasmania</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:ujacobs@durisol.com.au?subject=Durisol enquiry from website">ujacobs@durisol.com.au</a><br>Tel. 03 9758 5331<br>Mobile: 0400 417 043</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-victoria.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Uwe Jacobs</h5>
                  <p class="small">Victoria</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:ujacobs@durisol.com.au?subject=Durisol enquiry from website">ujacobs@durisol.com.au</a><br>Tel. 03 9758 5331<br>Mobile: 0400 417 043</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-western-australia.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Jim Moro</h5>
                  <p class="small">Western Australia</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:jmoro@durisol.com.au?subject=Durisol enquiry from website">jmoro@durisol.com.au</a><br>Tel. 08 6168 9266<br>Mobile: 0431 992 848</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-others.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Werner Weber</h5>
                  <p class="small">Other States</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:wweber@durisol.com.au?subject=Durisol enquiry from website">wweber@durisol.com.au</a><br>Tel: 08 6161 5580<br>Mobile: 0412 098 044</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section section-lg bg-gray-dark context-dark"> 
    <div class="shell">
      <h3>Schedule an Appointment</h3>
      <div class="range range-center"> 
        <div class="cell-sm-10 cell-md-8 cell-lg-7"> 
          <p class="text-center text-gray">Your email address will not be published. Required fields are marked with an asterisk symbol ( * )</p>
          <!-- RD Mailform-->
          {{-- <form class="rd-mailform text-left form-type-1 form-center" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
              @include('contact.form')
          </form> --}}
          {!! Form::open(['url' => '/contact', 'class' => 'text-left form-type-1 form-center','id'=>'formContact','enctype'=>'multipart/form-data']) !!}
            @include ('contact.form')
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </section>

@endsection