
	{{-- <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('name') ? ' has-error' : ''}}">           
                    {!! Form::text('name', null, ['placeholder' => 'Name']) !!} {!! $errors->first('name', '
                    <p class="help-block">:message</p>') !!}
        </div> 
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('email') ? ' has-error' : ''}}">           
                    {!! Form::email('email', null, ['placeholder' => 'Email']) !!} {!! $errors->first('email', '
                    <p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('phone') ? ' has-error' : ''}}">           
                    <input type="text" class="form-control" value="" id="phone" name="phone" placeholder="Phone" onkeypress="return isNumber(event)" />
                     {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>
        
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('subject') ? ' has-error' : ''}}">           
                    {!! Form::text('subject', null, ['placeholder' => 'Subject']) !!} {!! $errors->first('subject', '
                    <p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 {{ $errors->has('question') ? ' has-error' : ''}}">           
                     {!! Form::textarea('question', null, ['placeholder' => 'Type your question','id' => 'question','size' => '30x5']) !!}
                     {!! $errors->first('question', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">           
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'itg-button']) !!}
        </div>
    </div>     --}}

    <div class="form-wrap"> 
        <label class="form-label form-label-outside" for="contact-name">Your name *</label>
        <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
      </div>
      <div class="form-wrap">
        <label class="form-label form-label-outside" for="contact-email">Your e-mail *</label>
        <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
      </div>
      <div class="form-wrap">
        <label class="form-label form-label-outside" for="contact-message">Message *</label>
        <textarea class="form-input" id="contact-message" name="subject" data-constraints="@Required"></textarea>
      </div>
      <div class="form-wrap">
        <label class="form-label form-label-outside" for="contact-address">Address</label>
        <textarea class="form-input" id="contact-address" name="address" ></textarea>
      </div>
      <div class="form-wrap">
         <?php  $states = Config::get('setting.state'); ?>
        <label class="form-label form-label-outside" for="contact-message">State *</label>
        <Select class="form-input" id="contact-state" name="state" data-constraints="@Required">
            @foreach($states as $key => $value)
                <option value="{{$value}}" class="form-control" >{{$key}}</option>
            @endforeach
        </Select>
      </div>
      <div class="form-button group-sm text-center"> 
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'SUBMIT', ['class' => 'button button-primary']) !!}
      </div>

     <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
            

    </script>        