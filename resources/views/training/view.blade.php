@extends('layouts.master2')

 @section('title',$training->title)
@section('headerMeta')
<meta property="og:title" content="{{$training->title}}"/>
<meta property="og:description" content="{{$training->shortdesc}}"/>
<meta name="description" content="{{$training->shortdesc}}"/>
<meta name="author" content="itgeeksin.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">  
 <title>{{$training->title}}</title>     
@endsection 

@section('content')

        <section class="breadcrumb-wrapper breadcrumb-dark">
            <div class="shell">
              <ol class="breadcrumb-custom">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('/training')}}">Training</a></li>
                <li>{{$training->title}}
                </li>
              </ol>
            </div>
          </section>
          <section class="bg-gray-dark-2 section section-sm">
            <div class="shell">
              <div class="range range-60">
                <div class="cell-md-8">
                  <div class="single-post-block">
                    <h3 class="title-post">{{$training->title}}</h3>
                    <ul class="list-inline meta-list">
                      {{-- <li><span>By </span><a href="#">Admin</a></li> --}}
                      <li><span class="icon fl-outicons-clock169"></span>{{$training->created_at->format('d-m-Y')}}</li>
                      {{-- <li><span class="icon fl-outicons-speech-balloon2"></span><a href="#">3 comments</a></li> --}}
                    </ul>
                    <p>
                    @if(file_exists( public_path().'/Training/'.$training->image ) && $training->image !='')
                      <a href="{!!url("trainings/view/".$training->slug)!!}"><img src="{{asset ('Training/'.$training->image)}}" alt="{{$training->title}}" width="930" height="436"/></a>
                    @else
                      <a href="{!!url("Trainings/view/".$training->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$training->title}}" width="930" height="436"/></a>
                    @endif
                    </p>
                    <p> {!!$training->shortdesc!!} </p>
                    <p> {!!$training->longdescription!!}</p>
                    <div class="range">
                      @if(count($traininggallery) > 0)
                        @foreach($traininggallery as $gallery)
                          @if(file_exists( public_path().'/Training/thumb/'.$gallery->image ))
                            <div class="cell-sm-6">
                              <div class="box-project box-project-type-1">
                                <a target="_blank" href="{{ asset('Training/'.$gallery->image)}}"><img src="{{ asset('Training/'.$gallery->image)}}" alt="{{$gallery->training->title}}" width="460" height="169" ></a>
                              </div>
                            </div>
												
												{{-- <div class="work-gallery simple  col-md-6 col-sm-6 col-xs-12">
													<div class="theme-hover training_gallery">
														<figure>
															<img src="{{ asset('Training/thumb/'.$gallery->image)}}" alt="{{$gallery->training->title}}"/>
															<figcaption>
																<div class="content">
																	<div class="content-box">
																		<a href="{{ asset('Training/'.$gallery->image)}}" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a>
																	</div>
																</div>
															</figcaption>
														</figure>
													</div>
												</div> --}}
												
										  	@endif
                      @endforeach 
                    @else
                        No More Images
                                        
                    @endif 
                       
                    </div>

                    @if($training->video_url)
                    <div class="range">
                        <div class="cell-sm-6">
                          <div class="box-project box-project-type-1">
                              @if($training->video_type =='youtube')
                              <iframe width="640" height="360" src="{{$training->video_url}}" frameborder="0" allowfullscreen></iframe>
                              @else
                               <iframe src="{{$training->video_url}}" width="640" height="360" frameborder="0"></iframe>
                              @endif
                          </div>
                        </div>
                    </div>
                    @endif
                    
                   
                                
                   
                    {{-- <div class="single-post-footer">
                      <ul class="list-inline tag-list">
                        <li><span class="icon fl-outicons-tags"></span></li>
                        <li><a href="single-post.html">Design</a></li>
                        <li><a href="single-post.html">Remodeling</a></li>
                        <li><a href="single-post.html">Exterior</a></li>
                      </ul>
                      <ul class="list-inline share-list">
                        <li>Share:</li>
                        <li><a href="#">facebook</a></li>
                        <li><a href="#">twitter</a></li>
                        <li><a href="#">google+</a></li>
                      </ul>
                    </div> --}}
                  </div>
                   <div class="range">
                     @if($previous)
                    <div class="cell-sm-6">
                      <div class="box-project box-project-type-1">
                          @if(file_exists( public_path().'/Training/'.$previous->image ) && $previous->image !='')
                          <img src="{{asset ('Training/'.$previous->image)}}" alt="{{$previous->title}}" width="460" height="170"/>
                          @else
                          <img src="{{asset ('assets/images/default.jpg')}}" alt="{{$previous->title}}" width="460" height="170"/>
                          @endif
                      

                        <div class="caption"><a href="{!!url("trainings/view/".$previous->slug)!!}"><span class="heading-5">{{$previous->title}}</span><span class="small"> <span class="icon mdi-chevron-left mdi"></span>previous post </span></a></div>
                      </div>
                    </div>
                    @endif
                    @if($next)
                    <div class="cell-sm-6">
                      <div class="box-project text-right box-project-type-1">
                          @if(file_exists( public_path().'/Training/'.$next->image ) && $next->image !='')
                          <img src="{{asset ('Training/'.$next->image)}}" alt="{{$next->title}}" width="460" height="170"/>
                          @else
                          <img src="{{asset ('assets/images/default.jpg')}}" alt="{{$next->title}}" width="460" height="170"/>
                          @endif
                        <div class="caption"><a href="{!!url("trainings/view/".$next->slug)!!}"><span class="heading-5">{{$next->title}}</span><span class="small">next post<span class="icon mdi-chevron-right mdi"></span></span></a></div>
                      </div>
                    </div>
                    @endif
                  </div> 
                  {{-- <div class="comment-block">
                    <h5>2 Responses</h5>
                    <div class="comment-item">
                      <div class="unit unit-vertical unit-xs-horizontal">
                        <div class="unit__left"><img class="img-circle" src="images/single-post-4-120x120.jpg" width="120" height="120" alt=""></div>
                        <div class="unit__body">
                          <div class="comment-header">
                            <ul class="list-inline meta-list">
                              <li><span class="icon fl-outicons-user189"></span><a href="#">John Hohns</a></li>
                              <li><span class="icon fl-outicons-clock169"></span><a href="#">March 22, 2017 </a></li>
                            </ul><a class="icon fl-outicons-backward5 share-link" href="#">   </a>
                          </div>
                          <p class="text">Man, I always wanted something that cool in my yard!</p>
                        </div>
                      </div>
                    </div>
                    <div class="comment-item">
                      <div class="unit unit-vertical unit-xs-horizontal">
                        <div class="unit__left"><img class="img-circle" src="images/single-post-5-120x120.jpg" width="120" height="120" alt=""></div>
                        <div class="unit__body">
                          <div class="comment-header">
                            <ul class="list-inline meta-list">
                              <li><span class="icon fl-outicons-user189"></span><a href="#">Rodrigo Riverra </a></li>
                              <li><span class="icon fl-outicons-clock169"></span><a href="#">March 22, 2017 </a></li>
                            </ul><a class="icon fl-outicons-backward5 share-link" href="#"></a>
                          </div>
                          <p class="text">Yep, the tricks up the landscaping's sleeve just keep coming!  </p>
                        </div>
                      </div>
                    </div>
                  </div> --}}
                  {{-- <div class="range range-30 text-left">  
                    <div class="cell-xs-12">
                      <h5 class="text-white">Leave a reply</h5>
                    </div>
                    <div class="cell-sm-12">
                      <p class="text-left text-gray">Your email address will not be published. Required fields are marked with an asterisk symbol ( * )</p>
                      <!-- RD Mailform-->
                      <form class="rd-mailform text-left form-type-1" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                        <div class="form-wrap">
                          <label class="form-label form-label-outside text-white" for="contact-name">Your name *</label>
                          <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside text-white" for="contact-email1">Your e-mail *</label>
                          <input class="form-input" id="contact-email1" type="email" name="email" data-constraints="@Email @Required">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside text-white" for="contact-website">Your website *</label>
                          <input class="form-input" id="contact-website" type="text" name="website" data-constraints="@Required">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside text-white" for="contact-message">Comment *</label>
                          <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                        </div>
                        <div class="form-button group-sm text-left">
                          <button class="button button-primary" type="submit">Submit Comment</button>
                        </div>
                      </form>
                    </div>
                  </div> --}}
                </div>
                <div class="cell-md-4"> 
                  <div class="range range-center range-60 blog-sidebar">
                    {{-- <div class="cell-sm-6 cell-md-12">
                      <!-- RD Search-->
                      <div class="form-type-1"> 
                        <form class="rd-search" action="search-results.html" method="GET" data-search-live="rd-search-results-live">
                          <div class="rd-mailform-inline"> 
                            <div class="form-wrap">
                              <label class="form-label" for="rd-search-form-input">I'm looking for...</label>
                              <input class="form-input" id="rd-search-form-input" type="text" name="s" autocomplete="off">
                            </div>
                            <button class="button button-primary" type="submit">search</button>
                          </div>
                        </form>
                      </div>
                      <div class="about-box"><img class="img-circle" src="{{asset('/master/images/blog-5-450x450.jpg')}}" width="450" height="450" alt="">
                        <p>Quoteo Exterior Design company is a boutique professional design team based in Elmira and serving Waterloo, Kitchener, Guelph, and all neighboring local areas...</p>
                        <p class="text-white">The team at Quoteo has been providing some top-notch exterior solutions for both commercial and residential clients, with its history spanning over 12 years.</p>
                      </div>
                    </div> --}}
                    {{-- <div class="cell-sm-6 cell-md-12">
                      <div class="box-blog-contact bg-gray-dark">
                        <h5>Newsletter Subscription</h5>
                        <p>Sign up and keep yourself up-to-date with all the latest trends in the world of exterior design!</p>
                        <!-- RD Mailform-->
                        <form class="rd-mailform text-left form-type-2" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                          <div class="form-wrap">
                            <label class="form-label" for="contact-email">Your Email</label>
                            <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                            <button type="submit"><span class="icon fl-outicons-mail2"></span></button>
                          </div>
                        </form>
                      </div>
                      <div class="box-blog-contact bg-primary context-dark">
                        <h5>Follow Us</h5>
                        <p>Our exterior designers are posting exterior design news and ideas all the time!</p>
                        <div class="soc-block"><a class="icon icon-circle icon-circle-md icon-circle-gray fa-twitter" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-facebook-square" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-instagram" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-pinterest" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-youtube-play" href="#"></a></div>
                      </div>
                    </div> --}}
                    {{-- <div class="cell-sm-6 cell-md-12">
                      <h5 class="blog-aside-title"> customer quotes</h5>
                      <div class="unit unit-vertical unit-xs-horizontal quote-blog unit-md-vertical unit-lg-horizontal">
                        <div class="unit__left"><img class="img-circle" src="images/blog-6-132x132.jpg" width="132" height="132" alt=""></div>
                        <div class="unit__body">
                          <p class="big">I was very happy with the work this company did on my house. They were very professional and reliable.</p>
                          <h5 class="text-white">William Barnes</h5>
                          <p class="small text-uppercase text-primary">Freelance Programmer</p>
                        </div>
                      </div>
                    </div> --}}
                    {{-- <div class="cell-sm-6 cell-md-12"><a href="#"><img src="{{asset('master/images/banner.jpg')}}" alt=""></a></div> --}}
                    <div class="cell-sm-6 cell-md-12">
                        <h5 class="blog-aside-title">Training Categories</h5>
                        <ul class="list">
                          @if($trainingcategory)
                          @foreach($trainingcategory as $category)
                            <li><a href="#">{{$category->title}}</a></li>
                          @endforeach
                          @endif
                        </ul>
                    </div>

                    <div class="cell-sm-6 cell-md-12">
                        @if(count($recentTrainings) > 0)
                      <h5 class="blog-aside-title">Recent Trainings</h5>
                    
                      @foreach($recentTrainings as $training)
                      <div class="post-type-mini">
                        <div class="unit unit-vertical unit-xs-horizontal unit-md-vertical unit-lg-horizontal">
                          <div class="unit__left">
                              @if(file_exists( public_path().'/Training/'.$training->image ) && $training->image !='')
                                <a href="{!!url("trainings/view/".$training->slug)!!}"><img src="{{asset ('Training/'.$training->image)}}" alt="{{$training->title}}" width="122" height="122"/></a>
                              @else
                                <a href="{!!url("trainings/view/".$training->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$training->title}}" width="122" height="122"/></a>
                              @endif
                          </div>
                          <div class="unit__body">
                            <div class="post-time"><span class="icon fl-outicons-clock169"></span>{{ $training->created_at->format('d-m-Y') }}</div>
                            <div class="post-title"><a href="{!!url("trainings/view/".$training->slug)!!}">{{ $training->title }}</a></div>
                            <p> {!! \Illuminate\Support\Str::words($training->shortdesc, 15) !!} </p>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      @endif

                    </div>
                    {{-- <div class="cell-sm-6 cell-md-12">
                      <h5 class="blog-aside-title">Recent Comments</h5>
                      <ul class="list">
                        <li><a href="#">Taylor <span class="text-gray">on</span> What's a Hardscape, You Ask? Here's an Overview</a></li>
                        <li><a href="#">Derek  <span class="text-gray">on</span> 10 Expertly-Crafted Paint Schemes For Your Home Exterior</a></li>
                        <li><a href="#">Mary <span class="text-gray">on</span> Feng Shui Tips for Choosing House Exterior Color</a></li>
                        <li><a href="#">Emily <span class="text-gray">on</span> Exterior Paint: Satin or Flat Finish?</a></li>
                        <li><a href="#">Alvarez  <span class="text-gray">on</span> List of Exterior Wall Materials Using In Building Construction</a></li>
                      </ul>
                    </div> --}}
                   

                  </div>
                </div>
              </div>
            </div>
          </section>

@endsection  
