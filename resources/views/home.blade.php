@extends('layouts.master2')

@section('title','Home')

@section('content')
 <!-- Swiper-->
 <div class="swiper-container swiper-slider swiper-slider-type-3" data-loop="false" data-autoplay="false" data-simulate-touch="false">
    <div class="swiper-wrapper">
      
      @foreach($banners as $banner)
      <?php $banner_thumb_image = substr(strrchr($banner->image, '/'), 1); ?>
      @if(file_exists( public_path().'/banner/'.$banner_thumb_image ))
      <div class="swiper-slide" data-slide-bg="{{ asset('banner/'.$banner_thumb_image)}}">
        <div class="swiper-slide-caption">   
          <div class="shell">
            <div class="range"> 
              <div class="cell-md-9">
                <h2 data-caption-animate="fadeInUp" data-caption-delay="100">{{$banner->title}}</h2>
                <p data-caption-animate="fadeInUp" data-caption-delay="250">{{$banner->short_description}}</p>
                @if($banner->link)
                  <a class="button button-primary" href="{{$banner->link}}" data-caption-animate="fadeInUp" data-caption-delay="450">virtual tour</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      @endforeach                              
 
    </div>
    <!-- Swiper Pagination-->
    <div class="swiper-pagination"> </div>
  </div>	  

@if(count($freebanners) > 0)
  <section class="section section-two-column bg-secondary-2">
    <div class="block-left section-lg context-dark">
      <div class="box-content text-left">
        <div class="shell">
          <div class="range range-center range-md-left">
            <div class="cell-xs-10">
              <h3>{{$freebanners[0]->title}}</h3>
              <p>{{$freebanners[0]->description}}</p>
              <div style="margin-top:60px">
                <a id="freedownload" data-banners="{{$freebanners[0]->id}}" class="button button-circle button-primary button-icon button-icon-right" href="#"><span class="icon fa-download"></span>Get it now!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-right">
      <div class="img-fix">
        @if($freebanners[0]->feature_image)
        <img src="{{ asset('freebanner/'.$freebanners[0]->feature_image)}}" alt="{{$freebanners[0]->title}}" width="960" height="874" />
        @else
        <img src="{{ asset('master/images/about-1-960x874.jpg')}}" alt="{{$freebanners[0]->title}}" width="960" height="874" />
        @endif
      </div>
    </div>
  </section>
@endif

@if(count($freebanners) > 1)
  <section class="section section-two-column bg-primary">
    <div class="block-left">
      <div class="img-fix">
          @if($freebanners[1]->feature_image)
          <img src="{{ asset('freebanner/'.$freebanners[1]->feature_image)}}" alt="{{$freebanners[1]->title}}" width="960" height="874" />
          @else
          <img src="{{ asset('master/images/home-3-4-960x874.jpg')}}" alt="{{$freebanners[1]->title}}" width="960" height="874" alt="">
          @endif
        </div>
    </div>
    <div class="block-right section-lg context-dark">
      <div class="box-content text-left">
        <div class="shell">
          <div class="range range-center range-md-left">
            <div class="cell-xs-10">
              <h3>{{$freebanners[1]->title}}</h3>
              <p>{{$freebanners[1]->description}}</p>
              <div style="margin-top:60px">
      <a id="freedownload" data-banners="{{$freebanners[1]->id}}" class="button button-circle button-steel-blue button-icon button-icon-right" href="#"><span class="icon fa-download"></span>Get it now!</a>
      </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endif

@if(count($freebanners) > 2)
  <section class="section section-two-column bg-secondary-2">
    <div class="block-left section-lg context-dark">
      <div class="box-content text-left">
        <div class="shell">
          <div class="range range-center range-md-left">
            <div class="cell-xs-10">
              <h3>{{$freebanners[2]->title}}</h3>
              <p>{{$freebanners[2]->description}}</p>
              <div style="margin-top:60px">
      <a id="freedownload" data-banners="{{$freebanners[2]->id}}" class="button button-circle button-primary button-icon button-icon-right" href="#"><span class="icon fa-download"></span>Get it now!</a>
      </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-right">
      <div class="img-fix">
          @if($freebanners[2]->feature_image)
          <img src="{{ asset('freebanner/'.$freebanners[2]->feature_image)}}" alt="{{$freebanners[2]->title}}" width="960" height="874" />
          @else
          <img src="{{asset('master/images/about-1-960x874.jpg')}}" alt="{{$freebanners[2]->title}}" width="960" height="874" alt="">
          @endif
        </div>
    </div>
  </section>
@endif


  <section class="section bg-primary section-lg context-dark">
    <div class="shell">
      <h3>Durisol States Offices</h3>
      <div class="range">
        <div class="cell-xs-12">
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-carousel-light-dots owl-carousel-type-1" data-items="1" data-sm-items="2" data-md-items="4" data-dots="true" data-nav="true" data-stage-padding="15" data-loop="true" data-margin="30" data-mouse-drag="true" data-autoplay="true" data-autoplay-timeout="1000">
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-new-south-wales.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Andrew McLeod</h5>
                  <p class="small">New South Wales</p>
                </div>
      <p class="text"><a class="button button-circle button-steel-blue" href="mailto:amcleod@durisol.com.au?subject=Durisol enquiry from website">amcleod@durisol.com.au</a><br>Tel: 02 9868 4207<br>Mobile: 0416 138 943</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-queensland.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Philip Craig</h5>
                  <p class="small">Queensland</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:pcraig@durisol.com.au?subject=Durisol enquiry from website">pcraig@durisol.com.au</a><br>Tel. 07 5430 2221<br>Mobile: 0468 357 882</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-queensland-north.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Uwe Jacobs</h5>
                  <p class="small">Far North Queensland</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:ujacobs@durisol.com.au?subject=Durisol enquiry from website">ujacobs@durisol.com.au</a><br>Tel. 03 9758 5331<br>Mobile: 0400 417 043</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-south-australia.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>John Wauchope</h5>
                  <p class="small">South Australia</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:jwauchope@durisol.com.au?subject=Durisol enquiry from website">jwauchope@durisol.com.au</a><br>Mobile: 0430 302 680</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-tasmania.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Uwe Jacobs</h5>
                  <p class="small">Tasmania</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:ujacobs@durisol.com.au?subject=Durisol enquiry from website">ujacobs@durisol.com.au</a><br>Tel. 03 9758 5331<br>Mobile: 0400 417 043</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-victoria.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Uwe Jacobs</h5>
                  <p class="small">Victoria</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:ujacobs@durisol.com.au?subject=Durisol enquiry from website">ujacobs@durisol.com.au</a><br>Tel. 03 9758 5331<br>Mobile: 0400 417 043</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-western-australia.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Jim Moro</h5>
                  <p class="small">Western Australia</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:jmoro@durisol.com.au?subject=Durisol enquiry from website">jmoro@durisol.com.au</a><br>Tel. 08 6168 9266<br>Mobile: 0431 992 848</p>
              </div>
            </div>
            <div class="box-testimonials"><img class="img-circle" src="{{asset('master/images/australia-others.jpg')}}" width="180" height="180" alt="">
              <div class="caption">
                <div class="title">
                  <h5>Werner Weber</h5>
                  <p class="small">Other States</p>
                </div>
                <p class="text"><a class="button button-circle button-steel-blue" href="mailto:wweber@durisol.com.au?subject=Durisol enquiry from website">wweber@durisol.com.au</a><br>Tel: 08 6161 5580<br>Mobile: 0412 098 044</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


@endsection