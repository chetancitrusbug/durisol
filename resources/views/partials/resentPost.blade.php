<div class="col-xs-12 widget">
                                    <h3 class="widget_title">Recent Blogs</h3>
                                    
                                    <ul class="sidebar-post">
                                    @if(count($recentBlogs))
                                         @foreach($recentBlogs as $blog)
                                        <li>
                                            <div class="col-xs-4 padL0">
                                                <figure>
                                                     @if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='')
                                                        <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}"/></a>
                                                    @else
                                                        <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}"/></a>
                                                    @endif
                                                </figure>
                                            </div>
                                            <div class="col-xs-8 padL0">
                                                <p><a href="{!!url("blog/view/".$blog->slug)!!}">{{ $blog->title }}</a></p>
                                                <p>{{ $blog->created_at->diffForHumans() }}</p>
                                            </div>
                                        </li>   
                                        @endforeach 
                                    @else
                                       <li> No Blogs Found </li>
                                    @endif 

                                    </ul>
                                  
                                </div>