<!-- Main sidebar -->
<div class="sidebar sidebar-main">
        <div class="sidebar-content">
            <div class="sidebar-user">
                <div class="category-content">
                    <div class="media">
                        <div class="media-body">
                            <span class="media-heading text-semibold user-name text-center font-qrcode">Welcome User name</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
            $active = function ($menu) {
                $active = 0;
                if(str_contains(Request::url(), $menu->url) && ($menu->url!='' && $menu->url != "/admin" || (Route::getCurrentRoute()->uri() =="admin"))){
                    $active = 1;
                }
                if(!$active && isset($menu->items)){
                    foreach($menu->items as $me){
                        if(str_contains(Request::url(), $me->url) && ($me->url!='' && $me->url != "/admin" || (Route::getCurrentRoute()->uri() =="admin"))){
                            $active = 1;
                            break;
                        }
                    }
                }
                return $active;
            };

            $accessible = function ($menu) {
                $isaccessible = 0;
                // if($menu->allow_site == "master" && !_MASTER){
                //     $isaccessible = 0;
                // }else{
                //     $isaccessible = 1;
                // }
                if(isset($menu->permission) && Auth::user()->can($menu->permission)){
                    $isaccessible = 1;
                }else{
                    $isaccessible = 0;
                }
                $isaccessible = 1;
                return $isaccessible;
            };

            ?>

            <!-- Main navigation -->
            <div class="sidebar-category sidebar-category-visible">
                <div class="category-content no-padding">
                    <ul class="navigation navigation-main navigation-accordion">

                        @foreach($laravelAdminMenus->menus as $section)
                            
                           @if($accessible($section))
                           <li class="{{ ($active($section))? "active": "" }}">
                                <a class="{{ ($section->url)? "":"dropdown-collapse" }} {{ ($active($section))? "": "in" }}" href="{{ ($section->url) ? url($section->url) : "#"  }}"><i class="{{ $section->icon }}"></i>
                                    <span> {{$section->title}} </span>
                                    @if(isset($section->items) && count($section->items)>0) <i class="icon-angle-down angle-down"></i> @endif
                                </a>
                                @if(isset($section->items) && count($section->items)>0)
                                    <ul class="in nav nav-stacked {{ ($active($section))? "": "in" }}" style="display: {{ ($active($section))? "block": 'none' }}">
                                        @foreach($section->items as $menu)
                                            @if($accessible($menu))
                                            <li class="{{ ($active($menu))? "active": "" }}">
                                                <a href="{{ ($menu->url) ? url($menu->url) : "#"  }}"><i class="{{ $menu->icon }}"></i>
                                                    <span> {{$menu->title}}</span>
                                                </a>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                            @endif
            
            
                        @endforeach
                        <li>
                        <a href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                                  <i class="icon-switch2"></i>
                         Logout
                     </a>

                     <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                           style="display: none;">
                         {{ csrf_field() }}
                     </form>
                        </li>

                        {{-- <!-- Main -->
                        <!--<li class="navigation-header"><span>User Name</span> <i class="icon-menu" title="Main pages"></i></li>-->
                        <li class="active"><a href="index.html"><i class="icon-home4"></i><span>Home</span></a></li>
                        <li>
                            <a href=""><i class="fa fa-user"></i><span>My Profile</span></a>
                            <ul>
                                <li><a href="edit-profile.html"><i class="fa fa-edit sidebar-nav-icon"></i>Edit Profile</a></li>
                                <li><a href="change-password.html"><i class="fa fa-lock sidebar-nav-icon"></i>Change Password</a></li>
                            </ul>
                        </li>
                        
                        <li>
                            <a><i class="fa fa-list sidebar-nav-icon"></i><span> My Orders</span></a>
                            <ul>
                                <li><a href="order-list.html"><i class="fa fa-list-alt sidebar-nav-icon"></i>Orders List</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-qrcode sidebar-nav-icon"></i><span>My Qrcodes</span></a>
                            <ul>
                                <li><a href="generate-qr.html"><i class="fa fa-wrench sidebar-nav-icon"></i>Generate</a></li>
                                <li><a href="qrcode-list.html"><i class="fa fa-qrcode sidebar-nav-icon"></i>Qrcodes</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-shopping-cart sidebar-nav-icon"></i><span>Purchase Packages</span></a>
                        </li> --}}

                        <!-- /main -->
                    </ul>
                </div>
            </div>
            <!-- /main navigation -->

        </div>
    </div>
    <!-- /main sidebar -->