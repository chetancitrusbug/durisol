<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/images/durisol.png')}}" alt="logo" title="Durisol"></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                 <li class='dropdown dropdown-user'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

                        <i class="fa fa-user"></i>
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu dropdown-menu-right'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="icon-switch2"></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
                
{{--             
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span>User Name</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="edit-profile.html"><i class="icon-user-plus"></i> My profile</a></li>
                        <li><a href="#"><i class="fa fa-shopping-cart"></i>Purchase Packages</a></li>
                        
                        <li class="divider"></li>
                        
                        <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li> --}}
            </ul>
        </div>
    </div>
    <!-- /main navbar -->
