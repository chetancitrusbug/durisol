<footer class="page-footer footer-home-1">
    <div class="shell">
      <div class="range range-50">
        <div class="cell-xs-6 cell-md-3">
          <h5 class="footer-title">Categories</h5>
          <ul class="list">
            <li><a href="{{url('/about')}}">About</a></li>
            <li><a href="{{url('/blog')}}">Blog</a></li>
            <li><a href="{{url('/free-download')}}">Free Download</a></li> 
          </ul>
        </div>
        <div class="cell-xs-6 cell-md-3">
          <h5 class="footer-title">Contacts</h5>
          <address> 
            @if(isset($_setting[0]->address))
              <p>{{$_setting[0]->address}}</p>
            @endif
            @if(isset($_setting[0]->email))
              <a href="mailto:{{$_setting[0]->email}}">{{$_setting[0]->email}}</a>
            @endif
            {{-- <p>Corner Bannister/Baile Road<br>Canning Vale, WA, 6155, Australia</p><a href="mailto:admin@durisol.com.au">admin@durisol.com.au</a> --}}
          </address>
        </div>
        <div class="cell-xs-6 cell-md-3">
          <h5 class="footer-title">Opening hours</h5>
          <p>Mo-Fr 11:00-00:00 <br> Sa-Sun 15:00-00:00</p>
        </div>
        {{-- <div class="cell-xs-6 cell-md-3">
          <h5 class="footer-title">Tags</h5>
          <ul class="list-tag">
            <li><a href="#">Design</a></li>
            <li><a href="#">Exterior</a></li>
            <li><a href="#">Garden</a></li>
            <li><a href="#">Decorating</a></li>
            <li><a href="#">Hardscaping</a></li>
            <li><a href="#">Projects</a></li>
            <li><a href="#">Remodeling</a></li>
          </ul>
        </div> --}}
      </div>
    </div>
  </footer>

  <section class="footer-bottom">
      <div class="shell">
        <div class="range range-30 range-sm-middle">
          <div class="cell-sm-4 text-sm-left">
            <div class="footer__logo"><a href="{{url('/')}}"><img src="{{asset('/master/images/logo-white.png')}}" width="180" height="45" alt=""></a></div>
          </div>
          <div class="cell-sm-4">
            <p class="privacy">Durisol &#169; <span id="copyright-year"></span> &bull; <a href="{{url('privacy-policy')}}">Privacy Policy</a>
            </p>
          </div>
          <div class="cell-sm-4 text-sm-right">
            <div class="soc-block">
              @foreach($_setting as $settings)
                @if($settings->twitter)
                  <a class="icon icon-circle icon-circle-md icon-circle-gray fa-twitter" href="{{$settings->twitter}}" target="_blank"></a>
							  @endif
							  @if($settings->facebook)
                  <a class="icon icon-circle icon-circle-md icon-circle-gray fa-facebook-square" href="{{$settings->facebook}}" target="_blank"></a> 
								@endif
								@if($settings->insta)
                  <a class="icon icon-circle icon-circle-md icon-circle-gray fa-instagram" href="{{$settings->insta}}" target="_blank"></a>         
								@endif
								@if($settings->pintrest)
                  <a class="icon icon-circle icon-circle-md icon-circle-gray fa-pinterest" href="{{$settings->pintrest}}" target="_blank"></a>
								@endif
								@endforeach

              {{-- <a class="icon icon-circle icon-circle-md icon-circle-gray fa-youtube-play" href="#"></a> --}}
            </div>
          </div>
        </div>
      </div>
    </section>
  
