      <!-- Page Header-->
      <header class="page-header home-1-header">
          
          <!-- RD Navbar-->
          <div class="rd-navbar-wrap">
            <nav class="rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fullwidth" data-lg-device-layout="rd-navbar-fullwidth" data-md-stick-up-offset="137px" data-lg-stick-up-offset="137px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
              <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand"><a class="brand-name" href="{{url('/')}}"><img src="{{asset('master/images/logo-white.png')}}" width="180" height="45" alt=""></a></div>
                </div>
                <div class="rd-navbar-aside-right">
                  <div class="rd-navbar-nav-wrap">
                    <div class="phone-info">
                      <div class="unit unit-middle unit-horizontal unit-spacing-xs unit-xs-top">
                        <div class="unit__left"><span class="icon text-middle fl-bigmug-line-cellphone55"></span></div>
                        <div class="unit__body">
                          <div class="p">
                              @if(isset($_setting[0]->contactno))
                              <a class="text-middle" href="tel:{{$_setting[0]->contactno}}">{{$_setting[0]->contactno}}</a>
                              @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <address class="contact-info">
                      {{-- <ul class="group-lg">
                        <li>
                          <div class="unit unit-middle unit-horizontal unit-spacing-xs unit-xs-top">
                            <div class="unit__left"><span class="icon fl-bigmug-line-weekly15"></span></div>
                            <div class="unit__body">
                              <div class="p"><a class="header-link" href="schedule.html">Schedule an Appointment</a></div>
                            </div>
                          </div>
                        </li>
                      </ul> --}}
                    </address>
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class="active"><a href="{{url('/')}}">Home</a></li>
                      <li class=""><a href="{{url('/about')}}">About</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="{{url('/technical-info')}}">Technical Information</a></li>
                          <li><a href="#">References & Testimonials</a></li>
                        </ul>
                      </li>
                      <li class=""><a href="{{url('/blog')}}">Blogs</a>
                        {{-- <ul class="rd-navbar-dropdown">
                          <li><a href="#">Blog</a></li>
                          <li><a href="#">General Information</a></li>
                          <li><a href="#">Medias</a></li>
                        </ul> --}}
                      </li>
                      <li class=""><a href="{{url('/trainings')}}">Training</a>
                        {{-- <ul class="rd-navbar-dropdown">
                          <li><a href="#">Trainings</a></li>
                        </ul> --}}
                      </li>
                      <li class=""><a href="{{url('/testimonials')}}">Testimonials</a></li>
                      <li class=""><a href="{{url('/contact')}}">Contacts</a></li>
                      <li class=""><a href="{{url('/free-download')}}">Free Download Image</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </header>
@push('js')
<script>
    $(function(){
      var current_page_URL = location.href;
      $( "a" ).each(function() {
         if ($(this).attr("href") !== "#") {
           var target_URL = $(this).prop("href");
           if (target_URL == current_page_URL) {
              $('.rd-navbar-nav a').parents('li, ul').removeClass('active');
              $(this).parent('li').addClass('active');
              return false;
           }
         }
      });
    });
</script>
@endpush

  