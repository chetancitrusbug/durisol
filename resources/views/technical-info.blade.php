@extends('layouts.master2')

@section('title','Technical Information ')

@section('content')
<section class="breadcrumb-wrapper breadcrumb-dark">
        <div class="shell">
          <ol class="breadcrumb-custom">
					<li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{ url('/about') }}">about</a></li>
            <li>Technical Information</li>
          </ol>
        </div>
      </section>
      <section class="section bg-gray-dark-2">
        <div class="range range-condensed range-lg-reverse">
          <div class="cell-lg-6">
            <div class="section-sm box-content-right">
              <div class="shell">
                <div class="range text-left"> 
                  <div class="cell-lg-10 cell-lg-preffix-2">
                    <h3 class="text-white">Insulated Concrete Forms</h3>
                    <p>The Nexcem ICF wall system is a straightforward method that is used to build reinforced concrete walls. Walls built with ICF concrete feature a variety of helpful, protective, and energy-efficient properties for homes as well as commercial and industrial buildings. Some of these characteristics include thermal insulation, sound-proofing, and fire protection. To ensure long-lasting durability, The Nexcem ICF wall system is comprised of interlocking modular units that are dry-stacked (without mortar) and filled with concrete and reinforcing steel.</p>
					<p>Nexcem standard non-thermal wall form units have an insulation value of R8. This is based on the inherent insulation properties of the Nexcem material itself.</p>
					<p>Nexcem thermal wall form units have a range of insulation values from R14 to R28+ (thermal mass not included). The R-value for these components depends on the unit type. The higher R values are achieved by incorporated mineral fiber insulation inserts within the cavity of the ICF concrete unit at the time of manufacture. The thickness of the insert incorporated will determine the overall R value of the wall assembly.</p>
					<p>The choice to use mineral fiber insulation is deliberate because of its non-combustible and moisture resistant properties that complement the Nexcem material. The insulation inserts in our thermal blocks are positioned toward the exterior of the wall which results in additional protection and energy efficiency that is not possible with other insulated concrete forms. Since the R-values for our thermal units are based on the unit type, you can choose the optimal insulation value based on the requirements of your specific application. We can also incorporate other ICF concrete material inserts (such as poly-isocyanurate) for higher R-values upon special request. We can incorporate other insulation material inserts (such as poly-isocyanurate) for higher R-values upon special request.</p>
					<p>Nexcem Forms are designed to accommodate all practical ranges of concrete thickness. The load carrying capacity of the ICF wall system depends entirely upon the thickness of the concrete core and the steel reinforcing schedule. Standard reinforced concrete design principals (ACI 318 or CSA A23.3) based on the concrete cores are applicable.</p>
					<p>To learn more about our ICF wall systems and how they can benefit your next building project, please don’t hesitate to contact us to discuss your needs. Thanks for choosing Nexcem as your trusted source for environmentally friendly insulated concrete forms!</p>
					<p>Download a PDF version of the Nexcem System Summary here.</p>
					<h5 class="text-white">Wall System Summary</h5>
					<p>Nexcem Insulated Concrete Forms Dimensions</p>

					<div class="">
						<table class="table-custom table-primary table-fixed">
							<tr>
								<th>Overall Thickness</th>
								<th>Overall Height</th>
								<th>Overall Length</th>
								<th>Insulation Insert Included</th>
								<th>Total R-Value</th>
								<th>Concrete Core Thickness</th>
							</tr>
							<tr >
								<td>6"</td>
								<td>12"</td>
								<td>24"</td>
								<td>none</td>
								<td>R-8</td>
								<td>3.2"</td>
							</tr>
							<tr >
								<td>8"</td>
								<td>12"</td>
								<td>36"</td>
								<td>none</td>
								<td>R-8</td>
								<td>5"</td>
							</tr>
							<tr >
								<td>10"</td>
								<td>12"</td>
								<td>36"</td>
								<td>none</td>
								<td>R-8</td>
								<td>7"</td>
							</tr>
							<tr >
								<td>10"</td>
								<td>12"</td>
								<td>36"</td>
								<td>1.5"</td>
								<td>R-14</td>
								<td>5.5"</td>
							</tr>
							<tr >
								<td>12"</td>
								<td>12"</td>
								<td>24"</td>
								<td>none</td>
								<td>R-8</td>
								<td>8.5"</td>
							</tr>
							<tr >
								<td>12"</td>
								<td>12"</td>
								<td>24"</td>
								<td>1.5"</td>
								<td>R-14</td>
								<td>7"</td>
							</tr>
							<tr >
								<td>12"</td>
								<td>12"</td>
								<td>24"</td>
								<td>3"</td>
								<td>R-22</td>
								<td>5.5"</td>
							</tr>
							<tr >
								<td>14"</td>
								<td>12"</td>
								<td>24"</td>
								<td>none</td>
								<td>R-8</td>
								<td>10.5"</td>
							</tr>
							<tr >
								<td>14"</td>
								<td>12"</td>
								<td>24"</td>
								<td>1.5"</td>
								<td>R-14</td>
								<td>9"</td>
							</tr>
							<tr >
								<td>14"</td>
								<td>12"</td>
								<td>24"</td>
								<td>3"</td>
								<td>R-22</td>
								<td>7.5"</td>
							</tr>
							<tr >
								<td>14"</td>
								<td>12"</td>
								<td>24"</td>
								<td>5"</td>
								<td>R-28</td>
								<td>5.5"</td>
							</tr>
						</table>
					</div>
					<p>Basement walls and walls retaining more than 4ft of soil (unbalanced) typically require a minimum 6.5″ concrete core.</p>
					<p>Nexcem is the next generation of cement bonded wood fiber, taking all of the experience from the past and incorporating all of the technology from the present. The inherent insulation value, the 4 hour fire rating, the ease and speed of building and the moisture resistant capabilities make the system unmatched for most building projects. Nexcem Wall Forms have been used worldwide in every possible building application both above and below-grade:</p>
					
                    <ul class="list list-marked list-marked-inset-left">
                      <li>Residential (Single and Multi-Unit)</li>
                      <li>Industrial</li>
                      <li>Agricultural</li>
                      <li>Commercial</li>
                      <li>Institutional</li>
                      <li>High Rise (Over 26 story buildings in-place)</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="cell-lg-6">
            <div class="range range-condensed range-lg-reverse">
              <div class="cell-sm-12 cell-md-4 cell-lg-12">
                <div class="range range-condensed">
                  <div class="cell-xs-6 cell-sm-6 cell-md-12 cell-lg-6">
                    <div class="box-project"><a href="#"><img src="{{asset('master/images/nexcem-laying.jpg')}}" width="960" height="960" alt=""></a>
                      <div class="caption"><a href="#"><span class="heading-4">Arlington Plaza, Arlington, VA</span><span class="small">view projects  <span class="icon fl-bigmug-line-zoom60"></span></span></a></div>
                    </div>
                  </div>
                  <div class="cell-xs-6 cell-sm-6 cell-md-12 cell-lg-6">
                    <div class="box-project"><a href="#"><img src="{{asset('master/images/nexcem-preparing.jpg')}}" width="960" height="960" alt=""></a>
                      <div class="caption"><a href="#"><span class="heading-4">Waldorf Pestoria, Bay City, NY</span><span class="small">view projects  <span class="icon fl-bigmug-line-zoom60"></span></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell-xs-6 cell-md-8 cell-lg-12">
                <div class="box-project"><a href="#"><img src="{{asset('master/images/project-1-960x960.jpg')}}" width="960" height="960" alt=""></a>
                  <div class="caption"><a href="#"><span class="heading-4">The Bellevue Hotel, Fairview, NC</span><span class="small">view projects  <span class="icon fl-bigmug-line-zoom60"></span></span></a></div>
                </div>
              </div>
              <div class="cell-xs-6 cell-md-8 cell-lg-12">
                <div class="box-project"><a href="#"><img src="{{asset('master/images/single-service-5-960x960.jpg')}}" width="960" height="960" alt=""></a>
                  <div class="caption"><a href="#"><span class="heading-4">Hot Pancakes Bar, Belto, NJ</span><span class="small">view projects  <span class="icon fl-bigmug-line-zoom60">         </span></span></a></div>
                </div>
              </div>
              <div class="cell-sm-12 cell-md-4 cell-lg-12">
                <div class="range range-condensed">             
                  <div class="cell-xs-6 cell-sm-6 cell-md-12 cell-lg-6">
                    <div class="box-project"><a href="#"><img src="{{asset('master/images/durisol-sunshine-coast.jpg')}}" width="960" height="960" alt=""></a>
                      <div class="caption"><a href="single-service.html"><span class="heading-4">Johnson's Hotel & Casino, Las Vegas, NV</span><span class="small">view projects  <span class="icon fl-bigmug-line-zoom60"></span></span></a></div>
                    </div>
                  </div>
                  <div class="cell-xs-6 cell-sm-6 cell-md-12 cell-lg-6">
                    <div class="box-project"><a href="#"><img src="{{asset('master/images/durisol-thermal-performance.jpg')}}" width="960" height="960" alt=""></a>
                      <div class="caption"><a href="#"><span class="heading-4">Stanson Bar & BBQ, Chicago, IL</span><span class="small">view projects  <span class="icon fl-bigmug-line-zoom60"></span></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection