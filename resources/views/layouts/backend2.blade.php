<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard - Durisol</title>

	
    {{-- <link href="{{asset('assets/images/favicon.ico')}}" rel='icon' type='image/ico'> --}}
    
    <!-- favicons-->
     <link rel="apple-touch-icon" sizes="57x57" href="{{asset('master/images/favicons/apple-icon-57x57.png')}}">
     <link rel="apple-touch-icon" sizes="60x60" href="{{asset('master/images/favicons/apple-icon-60x60.png')}}">
     <link rel="apple-touch-icon" sizes="72x72" href="{{asset('master/images/favicons/apple-icon-72x72.png')}}">
     <link rel="apple-touch-icon" sizes="76x76" href="{{asset('master/images/favicons/apple-icon-76x76.png')}}">
     <link rel="apple-touch-icon" sizes="114x114" href="{{asset('master/images/favicons/apple-icon-114x114.png')}}">
     <link rel="apple-touch-icon" sizes="120x120" href="{{asset('master/images/favicons/apple-icon-120x120.png')}}">
     <link rel="apple-touch-icon" sizes="144x144" href="{{asset('master/images/favicons/apple-icon-144x144.png')}}">
     <link rel="apple-touch-icon" sizes="152x152" href="{{asset('master/images/favicons/apple-icon-152x152.png')}}">
     <link rel="apple-touch-icon" sizes="180x180" href="{{asset('master')}}/images/favicons/apple-icon-180x180.png">
     <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('master/images/favicons/android-icon-192x192.png')}}">
     <link rel="icon" type="image/png" sizes="32x32" href="{{asset('master/images/favicons/favicon-32x32.png')}}">
     <link rel="icon" type="image/png" sizes="96x96" href="{{asset('master/images/favicons/favicon-96x96.png')}}">
     <link rel="icon" type="image/png" sizes="16x16" href="{{asset('master/images/favicons/favicon-16x16.png')}}">
     <link rel="manifest" href="{{asset('master/images/favicons/manifest.json')}}">

	<!-- c stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <!-- Data Table -->
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
    <!-- Toastr -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    {{--  <!-- DarteRangePicker -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />  --}}
     {{--  <!-- DateTime Picker-->
    <link href="{!! asset('css/bootstrap-datetimepicker.css') !!}" media="all" rel="stylesheet" type="text/css"/>    --}}
    <!--select2-->
    <link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>
    {{--  <!-- Intelinput -->
    <link rel="stylesheet" href="{!! asset('assets/build/css/intlTelInput.css')!!}">  --}}
    <!-- include summernote css -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

    
    @stack('css')   

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/core/app.js')}}"></script>
	<!-- /theme JS files -->
	
    <!-- / jquery [required] -->
    <script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
    <!-- / jquery mobile (for touch events) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
    <!-- / jquery migrate (for compatibility with new jquery) [required] -->
    <script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
    <!-- / jquery ui -->
    <script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
    <!-- / jQuery UI Touch Punch -->
    <script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
            type="text/javascript"></script>
    <!-- / bootstrap [required] -->
    <script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>
    <!-- / modernizr -->
    <script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
    <!-- / retina -->
    <script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
    <!-- / theme file [required] -->
    <script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>
    <!-- Data Table-->
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
    <!-- Toatr -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- DateRangePicker  -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <!-- DateTime Picker-->
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}" type="text/javascript"></script>
    <!--select2-->
    <script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
    <!-- Custom JS -->
    <script src="https://triggrads.com/frontend/js/custom.js"></script>
    <!-- include summernote js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

</head>




<body>

        @include('partials.backend2.topbar')
    
        <!-- Page container -->
        <div class="page-container">
    
            <!-- Page content -->
            <div class="page-content">
    
                @include('partials.backend2.sidebar')
    
    
                <!-- Main content -->
                <div class="content-wrapper">
                
                    {{-- <section class="top-section">
                        <div class="page-header">
                            <div class="page-header-content">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 clearfix">
                                        <div class="page-title">
                                            <h4><span class="text-semibold font-head">@yield('title')</span></h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 clearfix">
                                    
                                    </div>
                                </div>
                            </div>			
                        </div><!-- /page header -->
    
                    </section> --}}
    
                    <!-- Content area -->
                    <div class="content">
                        <!-- Form horizontal -->

                                @if (Session::has('flash_message'))
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true">&times;</button>
                                        {{ Session::get('flash_message') }}
                                    </div>
                                @endif
                                @if (Session::has('flash_error'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true">&times;</button>
                                        {{ Session::get('flash_error') }}
                                    </div>
                                @endif
                                @if (Session::has('flash_success'))
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true">&times;</button>
                                        {{ Session::get('flash_success') }}
                                    </div>
                                @endif
            
                                @include('flash::message')

                        <div class="panel panel-flat">
                                
                                    <div class="panel-heading">
                                        <h5 class="panel-title order">@yield('title')</h5>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                            </ul>
                                        </div>
                                    </div>
    
                                    @yield('content')
                                
                            </div><!-- panel -->


                        
                        
                        <!-- dashboard row 1 -->
                        <div class="row">
                            {{-- <!-- start dashboard row 1 -->
                            <section class="content-section"> 

                                    
            
                                @yield('content')
                                
                            </section>
                            <!-- end of dashboard row 1 --> --}}
                                
                            @include('partials.backend2.footer')
                        </div>
                        <!-- /content area -->
                    </div>
                    <!-- /main content -->
    
                </div>
            <!-- /page content -->
    
        </div>
    
        </div>
        
        <!-- /page container -->
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">


    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
 <script>
	jQuery(".set_form").validate({
		
			rules: { 
				facebook: {
					 fb_link : true,
				},
                twitter: {
					 tw_link : true,
				},
                insta: {
					 insta_link : true,
				},
                pintrest: {
					 pin_link : true,
				}
			},
			messages: {
				facebook: {
					fb_link: "Please Enter Valid Facebook Link",
				},
                twitter: {
					tw_link: "Please Enter Valid Twitter Link",
				},
                insta: {
					insta_link: "Please Enter Valid Instagram Link",
				},
                pintrest: {
					pin_link: "Please Enter Valid Pinterest Link",
				}
		
			},
			submitHandler: function(form) {
				form.submit();
			}
		});

	jQuery.validator.addMethod(
	"fb_link",
	function(){
		var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		var facebook_url = jQuery(".facebook").val();
      
		if(facebook_url == null || facebook_url == ''){
			return true;
		}else{
			if (re.test(facebook_url)) 
				return true;
			else
				 return false;
			
		}
		
	}
);
jQuery.validator.addMethod(
    "tw_link",
	function(){
		var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		var tw_url = jQuery(".twitter").val();
      
		if(tw_url == null || tw_url == ''){
			return true;
		}else{
			if (re.test(tw_url)) 
				return true;
			else
				 return false;
			
		}
		
	}

);
jQuery.validator.addMethod(
	"insta_link",
	function(){
		var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		var insta_url = jQuery(".insta").val();
      
		if(insta_url == null || insta_url == ''){
			return true;
		}else{
			if (re.test(insta_url)) 
				return true;
			else
				 return false;
			
		}
		
	}
);
jQuery.validator.addMethod(
	"pin_link",
	function(){
		var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		var pin_url = jQuery(".pin").val();
		if(pin_url == null || pin_url == ''){
			return true;
		}else{
			if (re.test(pin_url)) 
				return true;
			else
				 return false;
			
		}
		
	}
);

  
/*
Valid Urls


"http://www.example.com"
"https://www.example.com"
"www.example.com"
*/

</script>

    <script>
            
                //Generator add button script
                $( document ).ready(function() {
                    $(document).on('click', '.btn-add', function(e) {
                        e.preventDefault();
            
                        var tableFields = $('.table-fields'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(tableFields);
            
                        newEntry.find('input').val('');
                        tableFields.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="fa fa-minus"></span>');
                    }).on('click', '.btn-remove', function(e) {
                        $(this).parents('.entry:first').remove();
            
                        e.preventDefault();
                        return false;
                    });
            
                });
            
            
                $(document).ready(function () {
            
            
                    $('#flash-overlay-modal').modal();
            
                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
            
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
            
                    $('.selectTag').select2({
                        tokenSeparators: [",", " "],
                    });
            
            
                    $('.time_pick').datetimepicker({
                        format: 'LT'
                    });
                  
                    $('.summernote').summernote({
                        height: 100,                 // set editor height
                        minHeight: null,             // set minimum height of editor
                        maxHeight: null,             // set maximum height of editor
                        focus: true,                  // set focus to editable area after initializing summernote
                        
                    });
                });
            
            
            </script>
    
@stack('js')    

</body>
</html>