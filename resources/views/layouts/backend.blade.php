 <!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Gesmansys' name='description'>
    {{--  <link href='{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}' rel='shortcut icon' type='image/x-icon'>  --}}

    <link href='{!! asset('assets/images/favicon.ico') !!}' rel='shortcut icon' type='image'>
    <link rel="stylesheet" href="{!! asset('/assets/stylesheets/plugins/fuelux/wizard.css') !!}">

    <!-- / END - page related stylesheets [optional] -->
    <!-- / bootstrap [required] -->
    <link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>

    <link href="{!! asset('css/bootstrap-datetimepicker.css') !!}" media="all" rel="stylesheet" type="text/css"/>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <!-- / theme file [required] -->
    <link href="{!! asset('assets/stylesheets/light-theme.css') !!}" media="all" id="color-settings-body-color"
          rel="stylesheet" type="text/css"/>
    <!-- / coloring file [optional] (if you are going to use custom contrast color) -->
    <link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!-- / demo file [not required!] -->
    <link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/design.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/style.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/imageuploadify.min.css') !!}" media="all" rel="stylesheet" type="text/css"/>
   
     
     
    <!--<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('assets/build/css/intlTelInput.css')!!}">
    
    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
    <style>
        #main-nav .navigation > .nav > li .nav {
            background: #333244 !important;
        }
        #main-nav .navigation > .nav > li .nav > li > a {
            color: #fff;
            font-size: 16px;
            padding-left: 25px;
            border-top: none;
            padding: 0px 0px 0px 25px;
        }
        #main-nav .navigation > .nav > li .nav > li.active > a , #main-nav .navigation > .nav > li .nav > li > a:hover, #main-nav .navigation > .nav > li .nav > li > a:focus {
            background: transparent;
            color: #17A9A8;
        }

        #main-nav .navigation > .nav > li > .nav > li {
            padding: 10px;
        }
    </style>

    @yield('headExtra')

    @stack('css')

     <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
     
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
</head>

<body class='contrast-red'>
<header>
    <nav class='navbar navbar-default'>
        <a class='navbar-brand' href='{!! url('/') !!}'>
            <img height="31" src="{{asset('assets/images/durisol.png')}}"/>
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>
        <ul class='nav'>


            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

                
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='icon-signout'></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
        </ul>
       
    </nav>
</header>
<div id='wrapper'>
    <div id='main-nav-bg'></div>

    @include('partials.sidebar')

    <section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <!-- <h1 class="pull-left">
                                    @yield('title','<i class="icon-tint"></i>
                                    <span>Home</span>')

                                </h1> -->
                            </div>
                        </div>
                    </div>

                    @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if (Session::has('flash_error'))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_error') }}
                        </div>
                    @endif
                    @if (Session::has('flash_success'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                    @include('flash::message')

                    @yield('content')


                </div>
            </div>
            <footer id='footer'>
                <div class='footer-wrapper'>
                    <div class='row'>
                        <div class='col-sm-6 text'>
                            Copyright @ 2017 {{ config('app.name') }}
                        </div>
                        <div class='col-sm-6 buttons'>

                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </section>
</div>
<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>

    <!-- DateTime Picker-->
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}" type="text/javascript"></script>


<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->


<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>

<!-- Data Table -->
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


<!-- Data Table-->
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="{!! asset('assets/build/js/intlTelInput.js')!!} " type="text/javascript"></script>



<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/javascripts/imageuploadify.js') !!}" type="text/javascript"></script>
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>

    //Generator add button script
    $( document ).ready(function() {
        $(document).on('click', '.btn-add', function(e) {
            e.preventDefault();

            var tableFields = $('.table-fields'),
                currentEntry = $(this).parents('.entry:first'),
                newEntry = $(currentEntry.clone()).appendTo(tableFields);

            newEntry.find('input').val('');
            tableFields.find('.entry:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="fa fa-minus"></span>');
        }).on('click', '.btn-remove', function(e) {
            $(this).parents('.entry:first').remove();

            e.preventDefault();
            return false;
        });

    });


    $(document).ready(function () {


        $('#flash-overlay-modal').modal();

        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.selectTag').select2({
            tokenSeparators: [",", " "],
        });


        $('.time_pick').datetimepicker({
            format: 'LT'
        });
      
        $('.summernote').summernote({
            height: 100,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true,                  // set focus to editable area after initializing summernote
            
        });
    });


</script>

@yield('footerExtra')
@stack('js')

<!-- / START - page related files and scripts [optional] -->

<!-- / END - page related files and scripts [optional] -->
</body>
</html> 

<?php /*?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard - Durisol</title>

	<link href="assets/images/favicon.png" rel="icon" type="image" />
	<link href="assets/images/favicon.ico" rel='icon' type='image/ico'>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backend/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('/backend/assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<!--<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>-->

	<script type="text/javascript" src="{{asset('/backend/assets/js/core/app.js')}}"></script>
	<!--<script type="text/javascript" src="assets/js/pages/dashboard.js"></script>-->
	<!-- /theme JS files -->


	
	<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>

<!-- / modernizr -->


<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>

<!-- Data Table -->
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


<!-- Data Table-->
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<!--select2-->
<link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>
<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('assets/build/css/intlTelInput.css')!!}">

</head>




<body>

	<!-- Main navbar -->
	<div class="navbar navbar-default header-highlight">
		<div class="navbar-header">
            <a class="navbar-brand" href="{{url('/')}}"><span class="media-heading text-semibold user-name text-center font-qrcode">DURISOL</span></a>
            {{-- <img src="{{asset('/assets/images/durisol.png')}}" alt="logo" title="QRcode"> --}}

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				
				<li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

					<i class="fa fa-user"></i>
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
													 <i class="icon-switch2"></i> 
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<div class="media-body">
									<span class="media-heading text-semibold user-name text-center font-qrcode">@if(Auth::check()){{\Auth::user()->name}}@endif</span>
								</div>
							</div>
						</div>
					</div>
					

					@include('partials.sidebar')

					
				

					{{-- <!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<!--<li class="navigation-header"><span>User Name</span> <i class="icon-menu" title="Main pages"></i></li>-->
								<li class="active"><a href="{{url('/')}}"><i class="icon-home4"></i><span>Home</span></a></li>
								<li>
									<a href=""><i class="fa fa-user"></i><span>My Profile</span></a>
									<ul>
										<li><a href="edit-profile.html"><i class="fa fa-edit sidebar-nav-icon"></i>Edit Profile</a></li>
										<li><a href="change-password.html"><i class="fa fa-lock sidebar-nav-icon"></i>Change Password</a></li>
									</ul>
								</li>
								
								<li>
									<a><i class="fa fa-list sidebar-nav-icon"></i><span> My Orders</span></a>
									<ul>
										<li><a href="order-list.html"><i class="fa fa-list-alt sidebar-nav-icon"></i>Orders List</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="fa fa-qrcode sidebar-nav-icon"></i><span>My Qrcodes</span></a>
									<ul>
										<li><a href="generate-qr.html"><i class="fa fa-wrench sidebar-nav-icon"></i>Generate</a></li>
										<li><a href="qrcode-list.html"><i class="fa fa-qrcode sidebar-nav-icon"></i>Qrcodes</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="fa fa-shopping-cart sidebar-nav-icon"></i><span>Purchase Packages</span></a>
								</li>

								<!-- /main -->
							</ul>
						</div>
					</div>
					<!-- /main navigation --> --}}

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
					
						<section class="top-section">
							<div class="page-header">
								<div class="page-header-content">
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12 clearfix">
											<div class="page-title">
												<h4><span class="text-semibold font-head"><a href="index.html">Dashboard</a> / @yield('title')</h4>
											</div>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-12 clearfix">
											
										</div>
									</div>
								</div>			
							</div><!-- /page header -->
		
						</section>
	
	
					<!-- Content area -->
					<div class="content">
	
						<!-- order-list -->
							<div class="panel panel-flat">
								
									<div class="panel-heading">
										<h5 class="panel-title order">@yield('title')</h5>
										<div class="heading-elements">
											<ul class="icons-list">
												<li><a data-action="collapse"></a></li>
											</ul>
										</div>
									</div>
									
									@yield('content')
									
								
								
							</div><!-- panel -->
							

							
						<!-- Footer -->
						<div class="footer text-muted">
							&copy; <a href="#" target="_blank">Copyright @ 2018 {{config('app.name')}} </a>
						</div>
						<!-- /footer --> 

						

					</div>
					<!-- /content area -->
				</div>
				<!-- /main content -->

			</div>
		<!-- /page content -->

	</div>

	</div>
	
	<!-- /page container -->
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>

    //Generator add button script
    $( document ).ready(function() {
        $(document).on('click', '.btn-add', function(e) {
            e.preventDefault();

            var tableFields = $('.table-fields'),
                currentEntry = $(this).parents('.entry:first'),
                newEntry = $(currentEntry.clone()).appendTo(tableFields);

            newEntry.find('input').val('');
            tableFields.find('.entry:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="fa fa-minus"></span>');
        }).on('click', '.btn-remove', function(e) {
            $(this).parents('.entry:first').remove();

            e.preventDefault();
            return false;
        });

    });


    $(document).ready(function () {


        $('#flash-overlay-modal').modal();

        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.selectTag').select2({
            tokenSeparators: [",", " "],
        });


        $('.time_pick').datetimepicker({
            format: 'LT'
        });
      
        $('.summernote').summernote({
            height: 100,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true,                  // set focus to editable area after initializing summernote
            
        });
    });


</script>

</body>
</html>
<?php */?>