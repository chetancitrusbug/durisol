<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <![endif]-->
        @if(View::hasSection('headerMeta'))
        @yield('headerMeta')

    @else
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Be-Photography | Creative Photography Agency, Portfolio and Multipurpose HTML5 Responsive Template" />
        <meta name="author" content="itgeeksin.com" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>@yield('title') | {{ config('app.name', 'Peninsuladrones') }}</title>
     @endif
        <!-- Bootstrap -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('frontend/assets/img/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{ asset('frontend/assets/img/favicon.ico')}}" type="image/x-icon">
        <!-- Master Css -->
        <link href="{{ asset('frontend/main.css')}}" rel="stylesheet">
        <link href="{{ asset('frontend/developer.css')}}" rel="stylesheet">
		    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">  
		<link href="{{ asset('frontend/style-custom.css')}}" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!--//================preloader  start==============//--> 
        <div class="preloader">
            <div class="itg-loader">
                <figure>
                    <img src="{{ asset('frontend/assets/img/loader/loder.png')}}" alt=""/>
                </figure>
                <h4>Loading</h4>
            </div>
        </div>
        <!--//================preloader  end==============//-->  
        <!--//================Header start==============//-->  
        <header id="header">
            <div id="main-menu" class="wa-main-menu">
                <!-- Menu -->
                <div class="wathemes-menu relative">
                    <!-- navbar -->
                    <div class="navbar navbar-default mar0" role="navigation">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 head-box">
                                    <div class="navbar-header">
                                        <!-- Button For Responsive toggle -->
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span> 
                                        <span class="icon-bar"></span> 
                                        <span class="icon-bar"></span> 
                                        <span class="icon-bar"></span>
                                        </button> 
                                        <!-- Logo -->
                                        <a class="navbar-brand" href="{!! url("/") !!}">
                                        <img class="site_logo" alt="Site Logo"  src="{{ asset('frontend/assets/img/logo.png')}}" />
                                        </a>
                                        <div class="head-search hidden-lg hidden-md hidden-sm ">
                                            <a href="#" class="search-click"><i class="fa fa-search" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <!-- Navbar Collapse -->
                                    <div class="navbar-collapse collapse">
                                        <!-- nav -->
                                       
                                        <ul class="nav navbar-nav">
                                            <li><a href="{!! url("/") !!}">home</a></li>
                                              <li>
                                                 <a href="{!! url("/about") !!}">about us</a>
                                                  <ul class="dropdown-menu">
                                                  
                                                    <li><a href="{!!url("/pricing")!!}">Pricing</a></li>                                                  
                                                  
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#">portfolio <i class="fa fa-angle-down hidden-lg hidden-md hidden-sm" aria-hidden="true"></i></a>
                                                <ul class="dropdown-menu">
                                                    @foreach($_portfolio_category as $cat_name)
                                                    <li><a href="{!!url("portfolio/view/".$cat_name->slug)!!}">{{$cat_name->title}}</a></li>                                                  
                                                    @endforeach
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="{{url('/blog')}}">blog</a> 
                                            </li>
                                            <li><a href="{{url('/contact')}}">contact</a></li>
                                            <li><a href="{{url('/free-download')}}" >Free Download</a></li>
                                        </ul>
                                    </div>
                                    <!-- navbar-collapse -->
                                </div>
                                <!-- col-md-12 -->
                            </div>
                            <!-- row -->
                        </div>
                        <!-- container -->
                    </div>
                    <!-- navbar -->
                    <div id="search-open" class="top-search">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    <input type="text" class="form-control" placeholder="Search">
                                    <a class="input-group-addon close-search search-close"><i class="fa fa-times"></i></a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  Menu -->
            </div>
        </header>

                   
    @yield('content')

            <!--//================Partner end==============//-->
            
        <div class="clear"></div>
        <!--//================Footer start==============//-->
        <footer class="main_footer">
            <div class="top_footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-box">
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="foot-sec">
                                    <a href="{{url('/')}}" class="footer-logo"><img class="site_logo" alt="Site Logo"  src="{{ asset ('frontend/assets/img/logo.png')}}" /></a>
                                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's;</p>
                                    <ul class="social-icon">
                                       @foreach($_setting as $seting)
									    @if($seting->twitter)
                                        <li><a href="{{$seting->twitter}}" class="active"><span><i class="fab fa-twitter" aria-hidden="true"></i></span></a></li>
										@endif
										@if($seting->facebook)
                                        <li><a href="{{$seting->facebook}}"  class="active"><span><i class="fab fa-facebook-f" aria-hidden="true"></i></span></a></li>
										@endif
										@if($seting->insta)
                                        <li><a href="{{$seting->insta}}"  class="active"><span><i class="fab fa-instagram" aria-hidden="true"></i></span></a></li>
										@endif
										@if($seting->pintrest)
                                        <li><a href="{{$seting->pintrest}}"  class="active"><span><i class="fab fa-pinterest" aria-hidden="true"></i></span></a></li>
										@endif
									@endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <div class="foot-sec">
                                    <ul class="footer-gallery">
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/A & Dogs Small.jpg')}}" alt="A & Dogs"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/Curacoa2_1 Small.jpg')}}" alt="Curacoa2_1"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/Hastings Marina Small.jpg')}}" alt="Hastings Marina"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/Locals Rule Small.jpg')}}" alt="Locals Rule"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/REDHILL18 Small.jpg')}}" alt="REDHILL18"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/SUBMARINE3 Small.jpg')}}" alt="SUBMARINE3"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/Surf on Shore Small.jpg')}}" alt="Surf on Shore"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/The Rip Small.jpg')}}" alt="The Rip"/></a></li>
                                        <li><a href="#"><img src="{{ asset ('frontend/assets/img/all/What The Small.jpg')}}" alt="What The"/></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-footer padTB20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <p>Copyright 2018  All Right Reserved | Made by <a href="https://www.totalsimplicit.com.au" target="_blank"><span class="theme-color">Total SimplicI-T</span></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Add Cash Add Modal -->
<div class="modal fade" id="freedownloadform" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="freedowmload modal-header">
                <h5 class="modal-title" id="account_form_model_lable">Free Download</h5> 
				 <p>Please fill in the form to receive a download link valid for 24 hour.</p> 
                <button type="button" class="freedowmload close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="freedownload_add_form" name="form">
                               
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                       
                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">Name</label>
                                            <input type="text" name="name" id="name" required>                                           
                                        </div>
                                         <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">Email</label>
                                            <input type="email" name="email" id="email" required>                                           
                                        </div>
                                         <input type="hidden" name="image" id="image" value="">  
                                    </div>
                                </div>
                                
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="Submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Cash Available Modal Close-->
        <!-- scroll top button -->
        <a id="scroll-top"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
        <!--//================Footer end==============//--> 	
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{ asset('frontend/assets/js/jquery.min.js')}}"></script>
        <script src="{{ asset('frontend/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('frontend/assets/plugin/megamenu/js/hover-dropdown-menu.js')}}"></script>
        <script src="{{ asset('frontend/assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js')}}"></script>
        <script src="{{ asset('frontend/assets/plugin/owl-carousel/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('frontend/assets/plugin/fancyBox/js/jquery.fancybox.pack.js')}}"></script>
        <script src="{{ asset('frontend/assets/plugin/fancyBox/js/jquery.fancybox-media.js')}}"></script> 
        <script src="{{ asset('frontend/assets/plugin/mixItUp/js/jquery.mixitup.js')}}"></script>        
        <script src="{{ asset('frontend/assets/js/main.js')}}"></script>
         <script src="{{ asset('frontend/assets/js/developer.js')}}"></script>
        <!--validate-->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script type="text/javascript">
 
    $("#formContact").validate({
        
        rules: { 
          
            name:{
                required:true
            },
            email:{
                required:true,
                email: true
            },
            phone:{
                required:true
            },
            subject:{
                required:true
            },
            question:{
                required:true
            },
            
        },
        messages: {
            
            name:{
                required:"Name Is required"
            },
            email:{
                required:"Email is required",
                email: "Enter Valid Email address"
            },
            phone:{
                required:"Phone No is required"
            },
            subject:{
                required:"Subject is required"
            },
            question:{
                required:"Question is required"
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    
  $(document).on('click', '#freedownload', function (e) {
         $('#freedownloadform').modal('show');  
        var imageid =$(this).attr('data-banners'); 
        $('#image').val(imageid); 
       
        }); 
        $("#freedownload_add_form").validate({
     
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
        },
        
    },
    submitHandler: function (form) {
        var frreedownload_url= "{{url('/freedownloaduser')}}";
        var method = "post";
        var str=$('#freedownload_add_form').serialize();
        $.ajax({
            type: method,
            url: frreedownload_url,
            data: $('#freedownload_add_form').serialize(),
            beforeSend: function () {
            },
                    
            success: function (data)
            {               
                data = JSON.parse(data)
               
                if(data.msg == 'Success'){
                    toastr.success('Thank You.Please check your email for image',data.message)
                }
                else{
                    toastr.error('Fail',data.message)
                }
                   
                $('#freedownloadform').modal('hide');
                location.reload();
            },
            error: function (error) {
                $('#freedownloadform').modal('hide');
            }
            
        });
        return false;      

    }
}); 
</script>
    </body>
</html>