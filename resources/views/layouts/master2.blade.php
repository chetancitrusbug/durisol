<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Site Title-->
    <title>@yield('title') | {{ config('app.name', 'Durisol') }}</title>
     <!-- Site Title-->
     <meta name="description" content="Durisol is a hight performance insulating ICF wall form / building block.">
 
     <meta name="format-detection" content="telephone=no">
     <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta charset="utf-8">
 
     <!-- Stylesheets-->
     <link rel="stylesheet" href="{{asset('master/css/montserrat.css')}}">
     <link rel="stylesheet" href="{{asset('master/css/bootstrap.css')}}">
     <link rel="stylesheet" href="{{asset('master/css/style.css')}}">
     <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
 
     <!-- favicons-->
     <link rel="apple-touch-icon" sizes="57x57" href="{{asset('master/images/favicons/apple-icon-57x57.png')}}">
     <link rel="apple-touch-icon" sizes="60x60" href="{{asset('master/images/favicons/apple-icon-60x60.png')}}">
     <link rel="apple-touch-icon" sizes="72x72" href="{{asset('master/images/favicons/apple-icon-72x72.png')}}">
     <link rel="apple-touch-icon" sizes="76x76" href="{{asset('master/images/favicons/apple-icon-76x76.png')}}">
     <link rel="apple-touch-icon" sizes="114x114" href="{{asset('master/images/favicons/apple-icon-114x114.png')}}">
     <link rel="apple-touch-icon" sizes="120x120" href="{{asset('master/images/favicons/apple-icon-120x120.png')}}">
     <link rel="apple-touch-icon" sizes="144x144" href="{{asset('master/images/favicons/apple-icon-144x144.png')}}">
     <link rel="apple-touch-icon" sizes="152x152" href="{{asset('master/images/favicons/apple-icon-152x152.png')}}">
     <link rel="apple-touch-icon" sizes="180x180" href="{{asset('master')}}/images/favicons/apple-icon-180x180.png">
     <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('master/images/favicons/android-icon-192x192.png')}}">
     <link rel="icon" type="image/png" sizes="32x32" href="{{asset('master/images/favicons/favicon-32x32.png')}}">
     <link rel="icon" type="image/png" sizes="96x96" href="{{asset('master/images/favicons/favicon-96x96.png')}}">
     <link rel="icon" type="image/png" sizes="16x16" href="{{asset('master/images/favicons/favicon-16x16.png')}}">
     <link rel="manifest" href="{{asset('master/images/favicons/manifest.json')}}">
     <meta name="msapplication-TileColor" content="#ffffff">
     <meta name="msapplication-TileImage" content="{{asset('master/images/favicons/ms-icon-144x144.png')}}">
     <meta name="theme-color" content="#ffffff">
     
     <!--[if lt IE 10]>
     <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
     <script src="js/html5shiv.min.js"></script>
     <![endif]-->
     
   </head>
  <body>
    <!-- Page-->
    <div class="text-center page">
      <!-- Page preloader-->
      <div class="page-loader"> 
        <div>
          <div class="page-loader-body"> 
            <div class="cssload-loader"> 
              <div class="cssload-inner cssload-one"> </div>
              <div class="cssload-inner cssload-two"></div>
              <div class="cssload-inner cssload-three"> </div>
            </div>
          </div>
        </div>
      </div>

      @include('partials.notifications')

      <!--  Page Header --> 
      @include('partials.master2.header')

      @yield('content')

      <!-- Free Download Modal --> 

      <div class="modal fade" id="freedownloadform" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="freedowmload modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Free Download</h5> 
                         <p>Please fill in the form to receive a download link valid for 24 hour.</p> 
                        <button type="button" class="freedowmload close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                                
                                <div class=" clearfix details-container details-port-container">
                                    <form method="post" id="freedownload_add_form" name="form">
                                       
                                        {{csrf_field()}} 
                                        <div class="form-group prepend-top">
                                            <div class="row">
                                               
                                                <div class="col-md-12">
                                                   
                                                    <input type="text" name="first_name" id="first_name" required class="form-control" placeholder="First Name">                                           
                                                </div>
                                                <div class="col-md-12">
                                                        
                                                    <input type="text" name="last_name" id="last_name" required class="form-control" placeholder="Last Name">                                           
                                                     </div>
                                                 <div class="col-md-12">
                                                   
                                                    <input type="email" name="email" id="email" required class="form-control" placeholder="Email">                                           
                                                </div>
                                                <div class="col-md-12">
                                                   
                                                    <input type="number" name="phone" id="phone" required class="form-control" placeholder="Phone">                                           
                                                </div>

                                                 <input type="hidden" name="image" id="image" value="">  
                                            </div>
                                        </div>
                                        
                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="btn btn-success account_form_submit_button" type="submit" name="submit" value="Submit">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
      <!-- Footer -->
      @include('partials.master2.footer')

    </div>

    <!-- Javascript-->
    <script src="{{asset('master/js/core.min.js')}}"></script>
    <script src="{{asset('master/js/script.js')}}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    @stack('js')
<script>

  
$(document).on('click', '#freedownload', function (e) {
       $('#freedownloadform').modal('show');  
      var imageid =$(this).attr('data-banners'); 
      $('#image').val(imageid); 
     
      }); 
      $("#freedownload_add_form").validate({
   
  rules: {
      first_name: {
          required: true,
      },
      last_name: {
        required: true,
      },
      email: {
          required: true,
          email :true
      }
  },
  submitHandler: function (form) {
      var frreedownload_url= "{{url('/freedownloaduser')}}";
      var method = "post";
      var str=$('#freedownload_add_form').serialize();
      $.ajax({
          type: method,
          url: frreedownload_url,
          data: $('#freedownload_add_form').serialize(),
          beforeSend: function () {
          },
                  
          success: function (data)
          {               
              data = JSON.parse(data)
             
              if(data.msg == 'Success'){
                  toastr.success('Thank You.Please check your email for image',data.message)
              }
              else{
                  toastr.error('Fail',data.message)
              }
                 
              $('#freedownloadform').modal('hide');
              location.reload();
          },
          error: function (error) {
              $('#freedownloadform').modal('hide');
          }
          
      });
      return false;      

  }
}); 
</script>
  </body>
</html>