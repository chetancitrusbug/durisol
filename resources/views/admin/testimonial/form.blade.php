<div class="form-group {{ $errors->has('business_name') ? 'has-error' : ''}}">
    {!! Form::label('business_name', 'Business Name *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('business_name', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('business_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('testimonial_text') ? 'has-error' : ''}}">
    {!! Form::label('testimonial_text', 'Testimonial Text *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('testimonial_text', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('testimonial_text', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('person_name') ? 'has-error' : ''}}">
    {!! Form::label('person_name', 'Person Name *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('person_name', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('person_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    {!! Form::label('date', 'Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label"></label>
        <div class="col-md-6">
    @if(isset($testimonial) && $testimonial->image != '')
    <img src="{{url('/Testimonial/'.$testimonial->image)}}" width="100" height="100"/>
    @endif
        </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
        @if(isset($testimonial) && $testimonial->image != '')
        {!! Form::label('image', 'Change Image', ['class' => 'col-md-4 control-label']) !!}
        @else
        {!! Form::label('image', 'Image', ['class' => 'col-md-4 control-label']) !!}
        @endif
    
    <div class="col-md-6">
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
