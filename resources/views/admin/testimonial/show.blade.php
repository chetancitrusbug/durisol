@extends('layouts.backend2')
@section('title','View Testimonial') 

@section('content')

                                           <div class="panel-body">

                        <a href="{{ url('/admin/testimonial') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/testimonial/' . $testimonial->id . '/edit') }}" title="Edit Testimonial"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/testimonial', $testimonial->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Testimonial',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $testimonial->id }}</td>
                                    </tr>
                                    <tr><th> Business Name </th><td> {{ $testimonial->business_name }} </td></tr><tr><th> Testimonial Text </th><td> {{ $testimonial->testimonial_text }} </td></tr><tr><th> Person Name </th><td> {{ $testimonial->person_name }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

@endsection
