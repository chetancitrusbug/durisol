@extends('layouts.backend2')
@section('title','Add Testimonial') 

@section('content')

                      <div class="panel-body">
                        <a href="{{ url('/admin/testimonial') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/testimonial', 'class' => 'form-horizontal col-md-6', 'files' => true]) !!}

                        @include ('admin.testimonial.form')

                        {!! Form::close() !!}

                    </div>

@endsection


