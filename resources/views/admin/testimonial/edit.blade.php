@extends('layouts.backend2')
@section('title','Edit Testimonial') 

@section('content')

                    <div class="panel-body">


                        <a href="{{ url('/admin/testimonial') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($testimonial, [
                            'method' => 'PATCH',
                            'url' => ['/admin/testimonial', $testimonial->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.testimonial.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>

@endsection
