@extends('layouts.backend2')
@section('title','Testimonials') 

@section('content')
 
                               <div class="panel-body">


                        <a href="{{ url('/admin/testimonial/create') }}" class="btn btn-success btn-sm" title="Add New Testimonial">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/testimonial', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Business Name</th><th>Testimonial Text</th><th>Person Name</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($testimonial) > 0 )
                                @foreach($testimonial as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->business_name }}</td><td>{{ $item->testimonial_text }}</td><td>{{ $item->person_name }}</td>
                                        <td>
                                            <a href="{{ url('/admin/testimonial/' . $item->id) }}" title="View Testimonial"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/testimonial/' . $item->id . '/edit') }}" title="Edit Testimonial"><button class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/testimonial', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Testimonial',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                <tr><td colspan=5> No TESTIMONIALS FOUND</td></tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $testimonial->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>

@endsection
