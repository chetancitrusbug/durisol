@extends('layouts.backend2')
@section('title','Add Training Category')
@section('pageTitle')
    <i class="icon-tint"></i>
    <span>Add New Training Category</span>
@endsection
@section('content')

            <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                    <br />
                    <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/admin/trainingcategory', 'class' => 'form-horizontal','id'=>'formBlogCategory']) !!}
                        @include ('admin.trainingcategory.form')
                    {!! Form::close() !!}
                </div>

@endsection

