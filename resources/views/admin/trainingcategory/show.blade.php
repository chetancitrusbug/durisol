@extends('layouts.backend2') 
@section('title','View Training Category') 
@section('content')

            <div class="panel-body">
                <a href="{{ URL::previous() }}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a> 
                <a href="{{ url('/admin/trainingcategory/' . $trainingcategory->id . '/edit') }}" title="Edit Training Category">
                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Edit Training Category
                    </button>
                </a>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                             <tr>
                                <td>Id</td>
                                <td>{{ $trainingcategory->id }}</td>
                            </tr> 

                            <tr>
                                <td>Name</td>
                                <td>{{ $trainingcategory->title }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>{{ $trainingcategory->status }}</td>
                            </tr>
                            

                        </tbody>
                    </table>
                </div>
            </div>

@endsection