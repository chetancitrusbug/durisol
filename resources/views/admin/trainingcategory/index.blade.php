@extends('layouts.backend2') 
@section('title','training Category') 
@section('pageTitle','Training Category') 
@section('content')

            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/admin/trainingcategory/create') }}" class="btn btn-success btn-sm" title="Add New Training Category">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Category </a>
                        </div>
                            <div class="col-md-6">
                                <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                                    <option value=''>All</option>
                                    <option value='active'>Active</option>
                                    <option value='inactive'>Inactive</option>
                                </select>
                            </div>
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/trainingcategory', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            {!! Form::close() !!}
                    </div>
                    <div class="table-responsive" >
                        <table class="table table-borderless" id="trainingcategory-table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

<script>
    $(function() { 
        var url ="{{ url('/admin/trainingcategory/') }}";
        var datatable = $('#trainingcategory-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("trainingCategoryData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { data: 'title',name:'title',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == 'inactive')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-edit' aria-hidden='true'></i> Active</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-edit' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);'><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;";   
                                                      
                            return v+e+d;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            
            var url ="{!! route('trainingCategoryData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/trainingcategory/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Category ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
