@extends('layouts.backend2') 
@section('title','View Portfolio') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Portfolio</div>
            <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> 
                    <a href="{{ url('/admin/portfolio/' . $portfolio->id . '/edit') }}" title="Edit Portfolio">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit Portfolio
                        </button>
                    </a>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $portfolio->id }}</td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>{{ $portfolio->title }}</td>
                            </tr>
                            <tr>
                                <td>Category</td>
                                <td>{{ $portfolio->categoryName->cat_title }}</td>
                            </tr>
                            <tr>
                                <td>Image</td>
                                <td><img src="{!! $portfolio->image !!}" alt="" width="100px"></td>
                            </tr>
                            <tr>
                                <td> Status</td>
                                <td>{{ $portfolio->status }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection