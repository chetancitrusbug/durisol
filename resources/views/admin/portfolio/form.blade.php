<div class="row">
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
                {!! Form::label('title', 'Title', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control']) !!} {!! $errors->first('title', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    @if(Route::currentRouteName() != 'portfolio.edit')
     {!! Form::hidden('categoryId', $categoryId, ['class' => 'form-control']) !!}
    @endif
    <!--<div class="col-sm-12 floatLeft">
            <div class="col-sm-6">   
                <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                    {!! Form::label('categoryId', 'Category', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-6">
                    {{ isset($cat) ? $cat : 'Default' }}
                        {!! Form::select('categoryId',$bannerCategory,null, ['class' => 'form-control']) !!}
                        {!! $errors->first('categoryId','<p class="help-block with-errors">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>-->
    @if(isset($portfolio) && $portfolio->image)   
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                {!! Form::label('image',  'View Image', ['class' => 'col-md-4 control-label']) !!}
                <img src="{!! $portfolio->image !!}" alt="Portfolio" width="100px" class="viewImage">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image',  'change image', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <input type="file" name="image" id="image" >
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image', 'Image *', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <input type="file" name="image" id="image" >
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>   
    @endif
@if(Route::currentRouteName() == 'portfolio.edit')
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">   
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('status',['active'=>'Active','inactive'=>'Inactive'] ,null, ['class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
@endif
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">   
            <div class="form-group {{ $errors->has('featured_portfolio') ? 'has-error' : ''}}">
                 {!! Form::label('', '', ['class' => 'col-sm-4 control-label']) !!}
                 <div class="col-sm-6">
                    {{ Form::checkbox('featured_portfolio', 1, null, ['class' => 'field']) }}Featured Portfolio
               </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">  
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
    </div> 
</div>                   