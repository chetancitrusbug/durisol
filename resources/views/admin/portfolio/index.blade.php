@extends('layouts.backend2') 
@section('title','Portfolio') 
@section('pageTitle','Portfolio') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i> Portfolio
                </div>
            </div>
            <div class="box-content ">
                <div class="row">
                    <div class="col-md-6">
                       
                    </div>
                        <div class="col-md-6">
                            <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                                <option value=''>All</option>
                                <option value='active'>Active</option>
                                <option value='inactive'>Inactive</option>
                            </select>
                        </div>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/Portfolio', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        {!! Form::close() !!}
                </div>
                <div class="table-responsive" >
                    <table class="table table-borderless" id="portfolio-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() { 
        var url ="{{ url('/admin/portfolio/') }}";
        var datatable = $('#portfolio-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route('portfolioData') !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							return o.title;
							
                        }
			        },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							var cat_name=o.category_name.cat_title;
							return cat_name;
                        }
			        } ,  
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							var img=o.image;
							if(img){
								return '<a href="'+o.image+'" target="_blank" ><img src="'+o.image+'" class="displayImage" ></a>';
                            }else{
								return 'No Image';
							}
                        }
			        }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == 'inactive')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Active</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;";   
                                                      
                            return v+e+d;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('portfolioData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/portfolio/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Portfolio ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
