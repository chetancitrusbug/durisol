@extends('layouts.backend2')

@section('title','Edit Image')
@section('pageTitle','Edit Image')


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Edit Image </div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($portfolio, [
                        'method' => 'PATCH',
                        'url' => ['/admin/portfolio', $portfolio->id],
                        'class' => 'form-horizontal',
                        'id'=>'formPortfolio',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.portfolio.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
