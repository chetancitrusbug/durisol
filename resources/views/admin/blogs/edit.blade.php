@extends('layouts.backend2')

@section('title','Edit Blog')
@section('pageTitle','Edit Blog')


@section('content')

                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($blog, [
                        'method' => 'PATCH',
                        'url' => ['/admin/blogs', $blog->id],
                        'class' => 'form-horizontal',
                        'id'=>'formBlog',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.blogs.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>

@endsection
