@extends('layouts.backend2') 
@section('title','Blogs') 
@section('pageTitle','Blogs') 
@section('content')

            
                <div class="panel-body-1 clearfix">
                    <div class="col-md-6">
                        <a href="{{ url('/admin/blogs/create') }}" class="btn btn-success btn-sm" title="Add New Blog">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Blog </a>
                    </div>
                    <div class="col-md-6">
                        <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                            <option value=''>All</option>
                            <option value='active'>Active</option>
                            <option value='inactive'>Inactive</option>
                        </select>
                    </div>
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/blogs', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    {!! Form::close() !!}
                  
                </div>
                <div class="panel-body">
                    
                <div class="table-responsive">
                    <table class="table table-borderless" id="blogstable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Video Type</th>
                                <th>Video Url</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                </div>
            

<script>
   $(function() { 
        var url ="{{ url('/admin/blogs/') }}";
        var imgurl ="{{ url('Blogs/thumb/') }}";
		var default_image="{{asset ('assets/images/default.jpg')}}";
        var datatable = $('#blogstable').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route('blogData') !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							return o.title;
							
                        }
			        },  
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							var img=o.image;
							if(img){
								return '<a href="'+imgurl+'/'+o.image+'" target="_blank" ><img src="'+imgurl+'/'+o.image+'" class="displayImage" width="100" height="100" ></a>';
                            }else{
								return '<a href="'+default_image+'" target="_blank" ><img src="'+default_image+'" class="displayImage" width="100" height="100" ></a>';
							}
                        }
			        }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							return o.video_type;
							
                        }
			        },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							return o.video_url;
							
                        }
			        },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == 'inactive')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-edit' aria-hidden='true'></i> Active</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-edit' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);'><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;";   
                                                      
                            return v+e+d;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('blogData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/blogs/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Blog?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection