@extends('layouts.backend2') 
@section('title','Free Download Image') 
@section('pageTitle','Free Download Image') 
@section('content')

            <div class="panel-body-1 clearfix">
                
                   
                        
            {!! Form::open(['method' => 'GET', 'url' => '/admin/imageuserlog', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            {!! Form::close() !!}
            </div>          
            <div class="panel-body">    
                <div class="table-responsive" >
                    <table class="table table-borderless" id="Banner-table">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>

<script>
    $(function() { 
        var url ="{{ url('/admin/banner/') }}";
        var datatable = $('#Banner-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route('imageuserlogData') !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'first_name',name:'first_name',"searchable" : true}, 
                    { data: 'last_name',name:'last_name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true},
                    { data: 'phone',name:'phone',"searchable" : true}, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							var img=o.image;
                         
                                if(img){
                                    return '<a href="'+imgurl+'/'+o.image+'" target="_blank" ><img src="'+imgurl+'/'+o.image+'" class="displayImage" style="width: 120px; height: 100px;" ></a>';
                                }else{
                                    return 'No Image';
                                }
                        
                        }
			        }, 
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";                           

                                d = "<a href='javascript:void(0);'><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                                      
                            return d;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('bannerData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/imageuserlog/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Banner ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
