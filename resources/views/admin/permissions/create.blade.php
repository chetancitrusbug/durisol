@extends('layouts.backend2')

@section('title','Create Permission')


@section('content')

                        <div class="panel-body">
                        <a href="{{ url('/admin/permissions') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/permissions', 'class' => 'form-horizontal','autocomplete'=>'off']) !!}

                        @include ('admin.permissions.form')

                        {!! Form::close() !!}

                    </div>

@endsection