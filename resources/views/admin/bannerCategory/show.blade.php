@extends('layouts.backend2') 
@section('title','View Banner Category') 
@section('content')

            <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> 
                    <a href="{{ url('/admin/bannerCategory/' . $bannerCategory->id . '/edit') }}" title="Edit Category">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i>
                          Edit  Banner Category 
                        </button>
                    </a>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $bannerCategory->id }}</td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>{{ $bannerCategory->title }}</td>
                            </tr>
                            <tr>
                                <td> Status</td>
                                <td>{{ $bannerCategory->status }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>

@endsection