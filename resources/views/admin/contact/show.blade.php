@extends('layouts.backend2') 
@section('title','View Contact Us') 
@section('content')

            <div class="panel-body">
                <a href="{{ URL::previous() }}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a> 
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                             <tr>
                                <td>Id</td>
                                <td>{{ $contact->id }}</td>
                            </tr> 

                            <tr>
                                <td>Name</td>
                                <td>{{ $contact->first_name }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{ $contact->email }}</td>
                            </tr>

                            <tr>
                                <td>Subject</td>
                                <td>{{ $contact->subject }}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>{{ $contact->address }}</td>
                            </tr>
                            <tr>
                                <td>State</td>
                                <td>{{ $contact->state }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

@endsection