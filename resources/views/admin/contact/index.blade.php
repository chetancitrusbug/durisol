@extends('layouts.backend2') 
@section('title','Contact Us') 
@section('pageTitle','Contact Us') 
@section('content')

            <div class="panel-body-1 clearfix">
                    
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/contact', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            {!! Form::close() !!}
            </div>  
            <div class="panel-body">
                
                    <div class="table-responsive" >
                        <table class="table table-borderless" id="contact-table">
                            <thead>
                                <tr>
                                  
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Message</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
            </div>
<script>
    $(function() { 
        var url ="{{ url('/admin/contact/') }}";
        var datatable = $('#contact-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("contactData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
               
                    { data: 'first_name',name:'first_name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true}, 
                    { data: 'subject',name:'subject',"searchable" : true},
                    { data: 'address',name:'address',"searchable" : true, "defaultContent": '-'},
                    { data: 'state',name:'state',"searchable" : true, "defaultContent": '-'},
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;";   
                                                      
                            return v+e+d;
                        }
                    }
                    
                ]
        });
        
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/contact/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Category ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
