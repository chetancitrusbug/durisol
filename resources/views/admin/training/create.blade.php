@extends('layouts.backend2')


@section('title','Add Training')



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>Add New Training</span>


    @endsection


@section('content')


                <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/training', 'class' => 'form-horizontal','id'=>'formBlog','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.training.form')

                        {!! Form::close() !!}

                    </div>

@endsection

