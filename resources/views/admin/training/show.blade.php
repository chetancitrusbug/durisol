@extends('layouts.backend2') 
@section('title','View Training') 
@section('content')

            <div class="panel-body">
                <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> @if(Auth::user()->can('access.training.edit'))
                <a href="{{ url('/admin/training/' . $training->id . '/edit') }}" title="Edit Training">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit training
                        </button>
                    </a> @endif {!! Form::open([ 'method' => 'DELETE', 'url' => ['/admin/training', $training->id],
                'style' => 'display:inline' ]) !!} {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete',
                array( 'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title' => 'Delete Training', 'onclick'=>'return
                confirm("Confirm delete?")' ))!!} {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            {{--  <tr>
                                <td>Id</td>
                                <td>{{ $blog->id }}</td>
                            </tr>  --}}

                            <tr>
                                <td>Name</td>
                                <td>{{ $training->title }}</td>
                            </tr>
                            <tr>
                                <td> Description</td>
                                <td>{{ $training->longdescription }}</td>
                            </tr>
                            @if($training->shortdesc)
                            <tr>
                                <td>Short Description</td>
                                <td>{{ $training->shortdesc }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>Video Type</td>
                                <td>{{ $training->video_type }}</td>
                            </tr>
                            <tr>
                                <td>Video Url</td>
                                <td>{{ $training->video_url }}</td>
                            </tr>
                            <tr>
                                <td>Feature Image</td>
                                 @if(isset($training->image))
                                <td><img src="{!! asset('Training/'.$training->image) !!}" style="height:50px;width:50px;"></td>
                                @else
                                <td><img src="{{asset ('assets/images/default.jpg')}}" style="height:50px;width:50px;"></td>
                                @endif
                            </tr>
                            @if(isset($images) && $images)
                            <tr>
                                <td>Other Image</td>
                                <td>
                                    @foreach($images as $img_name)
                                    <img src="{!! asset('Training/'.$img_name->image) !!}" style="height:50px;width:50px;">                                    @endforeach
                                </td>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>

@endsection