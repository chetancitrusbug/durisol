@extends('layouts.backend2') 
@section('title','View Banner') 
@section('content')

            <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> 
                    <a href="{{ url('/admin/banner/' . $banner->id . '/edit') }}" title="Edit Banner">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit Banner
                        </button>
                    </a>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $banner->id }}</td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>{{ $banner->title }}</td>
                            </tr>
                            <tr>
                                <td>Short Description</td>
                                <td>{{ $banner->short_description }}</td>
                            </tr>
                            <tr>
                                <td>Link</td>
                                <td>{{ $banner->link }}</td>
                            </tr>
                            <tr>
                                <td>Image</td>
                                <td><img src="{!! $banner->image !!}" alt="" width="100px"></td>
                            </tr>
                            <tr>
                                <td> Status</td>
                                <td>{{ $banner->status }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>

@endsection