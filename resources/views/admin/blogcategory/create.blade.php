@extends('layouts.backend2')
@section('title','Add Blog Category')
@section('pageTitle')
    <i class="icon-tint"></i>
    <span>Add New Blog Category</span>
@endsection
@section('content')

            <div class="box-content panel-body">
                    <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                    <br />
                    <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/admin/blogcategory', 'class' => 'form-horizontal','id'=>'formBlogCategory']) !!}
                        @include ('admin.blogcategory.form')
                    {!! Form::close() !!}
                </div>

@endsection

