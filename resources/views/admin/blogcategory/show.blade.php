@extends('layouts.backend2') 
@section('title','View Blog Category') 
@section('content')

            <div class="panel-body">
                <a href="{{ URL::previous() }}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a> 
                <a href="{{ url('/admin/blogcategory/' . $blogcategory->id . '/edit') }}" title="Edit Blog Category">
                    <button class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i>
                        Edit Blog Category
                    </button>
                </a>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                             <tr>
                                <td>Id</td>
                                <td>{{ $blogcategory->id }}</td>
                            </tr> 

                            <tr>
                                <td>Name</td>
                                <td>{{ $blogcategory->title }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>{{ $blogcategory->status }}</td>
                            </tr>
                            

                        </tbody>
                    </table>
                </div>
 
@endsection