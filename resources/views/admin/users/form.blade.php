@push('css')
<style>
    .intl-tel-input{
        display: block;
    }
</style>
@endpush


    <div class="col-md-4 col-sm-offset-1">
     
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="">
                <span class="field_compulsory">*</span>Name
            </label>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

        </div>
       
    
        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>Role
            </label>
            <div >
                {!! Form::select('roles[]',$roles,isset($user_roles) ? $user_roles : [], ['class' => 'full-width select2', 'multiple' => true]) !!}
            </div>
        </div>

    </div>

    <div class="col-md-4 col-sm-offset-1">
   
        
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="">
                <span class="field_compulsory">*</span>Email
            </label>
                 {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}

        </div>
        
        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password" class="">
                @if(!isset($user_roles))
                    <span class="field_compulsory">*</span>
                @endif
               Password
            </label>
                 {!! Form::password('password', ['class' => 'form-control']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}

        </div>
    </div>


<div class="form-group">
    <div class="col-md-offset-8 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        {{--  {{ Form::reset('Clear', ['class' => 'btn btn-primary']) }}  --}}
    </div>

</div>




