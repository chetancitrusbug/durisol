@extends('layouts.backend2')

@section('title','Users')
@section('pageTitle','Users')

@section('content')

<div class="panel-body-1 clearfix">
    
    <div class="col-md-6">
        {{--  @if(Auth::user()->can('access.user.create'))
                <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm" title="Add New User"> <i class="fa fa-plus" aria-hidden="true"></i>Add New</a>
            @endif  --}}
    </div>
    {{-- <div class="col-md-6">
    {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-search">
            <input type="search" class="form-control search" name="search" placeholder="{{Request::get('search')}}" value="{!! request()->get('search') !!}">
        </div>
    {!! Form::close() !!} 
    </div>                   --}}
</div>
<div class="panel-body">
    <div class="table-responsive">
        <table class="table table-borderless datatable responsive">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td> {{$user->id}}</td>
                    <td> {{$user->name}}</td>
                    <td> {{$user->email}}</td>
                    <td> {{join(' + ', $user->roles()->pluck('label')->toArray())}}</td>
                    <td>

                            <a href="{{ url('/admin/users/' . $user->id) }}" title="View User">
                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                       aria-hidden="true"></i> View
                                </button>
                            </a>

                            @if(Auth::user()->can('access.user.edit'))

                                <a href="{{ url('/admin/users/' . $user->id . '/edit') }}"
                                   title="Edit User">
                                    <button class="btn btn-primary btn-xs"><i class="fa fa-edit"
                                                                              aria-hidden="true"></i> Edit
                                    </button>
                                </a>
                            @endif

                            @if(Auth::user()->can('access.user.delete'))

                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'url' => ['/admin/users', $user->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete User',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            @endif

                        </td>
                </tr>   
                @endforeach
            </tbody>

        </table>
    </div>

    <div class="pagination-wrapper"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>

</div>

@endsection




