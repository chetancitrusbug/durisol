@extends('layouts.backend2')

@section('title','Edit User')
@section('pageTitle','Edit User')


@section('content')

                <div class="panel-body">
                    <a href="{{ url('/admin/users') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                     
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($user, [
                        'method' => 'PATCH',
                        'url' => ['/admin/users', $user->id],
                        'class' => 'form-horizontal',
                        'files' => true,
                        'autocomplete'=>'off'
                    ]) !!}

                    @include ('admin.users.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    

                </div>

@endsection