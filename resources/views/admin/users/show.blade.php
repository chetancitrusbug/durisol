@extends('layouts.backend2') 
@section('title', 'Show User') 
@section('pageTitle','Datail Of User')

@section('content')


            <div class="panel-body">
                <a href="{{ url('/admin/users') }}" title="Back" class="">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a>
               
                @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                           edit
                        </button>
                    </a>
                @endif
                 {!! Form::open([ 'method' => 'DELETE', 'url' => ['/admin/users', $user->id],
                    'style' => 'display:inline' ]) !!} {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete', array( 'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title' => 'Delete User', 'onclick'=>"return
                    confirm('Confirm delete?')" ))!!} {!! Form::close() !!}
                   
                    <br/>
                    <br/>

                    <?php
                    $role = join(' + ', $user->roles()->pluck('label')->toArray());
                    ?>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td> Id </td>
                                            <td>{{ $user->id }}</td>
                                        </tr>

                                        <tr>
                                            <td> Name </td>
                                            <td>{{ $user->name }}</td>
                                        </tr>


                                        <tr>
                                            <td> Email </td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Role </td>
                                            <td>{{ $role }}</td>
                                        </tr>



                                    </tbody>
                                </table>
                            </div>
                        </div>


@endsection