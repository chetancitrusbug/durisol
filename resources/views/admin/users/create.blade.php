@extends('layouts.backend2')


@section('title','Create User')



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>Create User</span>


    @endsection


@section('content')

                        <div class="panel-body">
                        <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/users', 'class' => 'form-horizontal','files' => true,'autocomplete'=>'off']) !!}

                        @include ('admin.users.form')

                        {!! Form::close() !!}

                    </div>

@endsection
