<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description',null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('event_date') ? 'has-error' : ''}}">
    {!! Form::label('event_date', 'Event Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('event_date', null, ['class' => 'form-control','id' => 'event_date']) !!}
        {!! $errors->first('event_date', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('event_address_1') ? 'has-error' : ''}}">
    {!! Form::label('event_address_1', 'Event Address 1 *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('event_address_1', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('event_address_1', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('event_address_2') ? 'has-error' : ''}}">
    {!! Form::label('event_address_2', 'Event Address 2', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('event_address_2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('event_address_2', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('city', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
    {!! Form::label('state', 'State *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('state', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label"></label>
        <div class="col-md-6">
    @if(isset($event) && $event->feature_image != '')
    <img src="{{url('/Events/'.$event->feature_image)}}" width="100" height="100"/>
    @endif
        </div>
</div>
<div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
    @if(isset($event) && $event->feature_image != '')
    {!! Form::label('feature_image', 'Change Feature Image', ['class' => 'col-md-4 control-label']) !!}
    @else
    {!! Form::label('feature_image', 'Feature Image *', ['class' => 'col-md-4 control-label']) !!}
    @endif
    <div class="col-md-6">
        {!! Form::file('feature_image', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('js')
<script>
$(function () {
    $('#event_date').datetimepicker({
        format: 'DD/MM/YYYY',
        minDate: new Date(),
        
    });
})
</script>
@endpush
