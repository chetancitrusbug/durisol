@extends('layouts.backend2')
@section('title','Edit Event')

@section('content')

                    <div class="panel-body">


                        <a href="{{ url('/admin/events') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($event, [
                            'method' => 'PATCH',
                            'url' => ['/admin/events', $event->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.events.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>

@endsection
