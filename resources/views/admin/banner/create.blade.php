@extends('layouts.backend2')
@section('title','Add Banner')
@section('pageTitle')
    <i class="icon-tint"></i>
    <span>Add New Banner</span>
@endsection
@section('content')

            <div class="box-content panel-body">
                    <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                    <br />
                    <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/admin/banner', 'class' => 'form-horizontal','id'=>'formBanner','enctype'=>'multipart/form-data']) !!}
                        @include ('admin.banner.form')
                    {!! Form::close() !!}
                </div>

@endsection

