@extends('layouts.backend2') 
@section('title','Banner') 
@section('pageTitle','Banner') 
@section('content')

            
                <div class="panel-body-1 clearfix">
                    <div class="col-md-6">
                        <a href="{{ url('/admin/banner/create') }}" class="btn btn-success btn-sm" title="Add New Banner">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Banner </a>
                    </div>
                    
                        <div class="col-md-6">
                            <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                                <option value=''>All</option>
                                <option value='active'>Active</option>
                                <option value='inactive'>Inactive</option>
                            </select>
                        </div>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/banner', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        {!! Form::close() !!}
                </div>
                <div class="panel-body">
                <div class="table-responsive" >
                    <table class="table table-borderless" id="Banner-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
								<th>Sort No</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
                </div>
            
 
<script>
    $(function() { 
        var url ="{{ url('/admin/banner/') }}";
        var datatable = $('#Banner-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route('bannerData') !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { data: 'title',name:'banner.title',"searchable" : true}, 
					//{ data: 'sort_no',name:'banner.sort_no',"searchable" : true},
                    { 
                        "data" : null, 
                        "name" : 'sort_no',
                        "searchable" : false,
                        render : function(o){
                            
                                sort_no = "<input type='number' name='sort_no' id='sort_no' data-id='"+o.id+"'value='"+o.sort_no+"'>"; 

                            return sort_no;
                            
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
							var img=o.image;
							if(img){
								return '<a href="'+o.image+'" target="_blank" ><img src="'+o.image+'" class="displayImage" width="100" height="100" ></a>';
                            }else{
								return 'No Image';
							}
                        }
			        }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == 'inactive')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-edit' aria-hidden='true'></i> Active</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-edit' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;";   
                                                      
                            return v+e+d;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('bannerData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/banner/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Banner ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                     headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });

        $("#Banner-table").on("focusout", "#sort_no", function () {
            var value = $(this).val();
            var id = $(this).attr('data-id');
            var url =  "{{url('admin/banner')}}/"+id+"?sort_no="+value ;
            console.log(url);
            if(value != null){
                $.ajax({
                    type: "get",
                    url: url,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success("Sort No Changed Successfully", data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
