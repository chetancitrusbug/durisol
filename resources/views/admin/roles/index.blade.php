@extends('layouts.backend2')

@section('title','Roles')

@section('pageTitle','Roles')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Roles
                    </div>
                    {{--  <div class="actions">
                        @include('partials.page_tooltip',['model' => 'role','page'=>'index'])
                    </div>  --}}
                </div>
                <div class="box-content ">

                    <div class="row">


                        <div class="col-md-6">

                            @if(Auth::user()->can('access.role.create'))
                                <a href="{{ url('/admin/roles/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Role">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>
                            @endif
                        </div>

                        <div class="col-md-3">

                        </div>

                        
                    </div>

</div>
<div class="box-content panel-body">



                    <div class="row">
                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Label</th>                                
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                                    @foreach($roles as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td><a href="{{ url('/admin/roles', $item->id) }}">{{ $item->name }}</a></td>
                                            <td>{{ $item->label }}</td>
                                            <td>
                                                <a href="{{ url('/admin/roles/' . $item->id) }}" title="View Role">
                                                    <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i> View
                                                    </button>
                                                </a>
        
                                               
                                                    @if(Auth::user()->can('access.role.edit'))
                                                        <a href="{{ url('/admin/roles/' . $item->id . '/edit') }}"
                                                           title="Edit Role">
                                                            <button class="btn btn-primary btn-xs"><i
                                                                        class="fa fa-pencil-square-o"
                                                                        aria-hidden="true"></i> Edit
                                                            </button>
                                                        </a>
                                                    @endif
        
                                                    @if(Auth::user()->can('access.role.delete'))
                                                        {!! Form::open([
                                                            'method' => 'DELETE',
                                                            'url' => ['/admin/roles', $item->id],
                                                            'style' => 'display:inline'
                                                        ]) !!}
                                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                                'type' => 'submit',
                                                                'class' => 'btn btn-danger btn-xs',
                                                                'title' => 'Delete Role',
                                                                'onclick'=>'return confirm("Confirm delete?")'
                                                        )) !!}
                                                        {!! Form::close() !!}
                                                    @endif
                                               
        
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                        </table>
                        
                    </div>
</div>


                </div>
            </div>
        </div>
    </div>
@endsection


