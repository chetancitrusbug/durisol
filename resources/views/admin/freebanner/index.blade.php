@extends('layouts.backend2') 
@section('title','Free Download Image') 
@section('pageTitle','Free Download Image') 
@section('content')

            <div class="panel-body-1 clearfix">
                
                    <div class="col-md-6">
                        <a href="{{ url('/admin/freebanner/create') }}" class="btn btn-success btn-sm" title="Add New Download Image">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Free Download Image </a>
                    </div>
                    
                       
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/freebanner', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        {!! Form::close() !!}
            </div>
            <div class="panel-body">    
                <div class="table-responsive" >
                    <table class="table table-borderless" id="Banner-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Featured</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
           

<script>
    $(function() { 
        var url ="{{ url('/admin/freebanner/') }}";
        var imgurl ="{{ url('freebanner/thumb/') }}";
        var datatable = $('#Banner-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route('freebannerData') !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'title',name:'title',"searchable" : true}, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
                            var img=o.image;
                            var type =o.type;
                            if(o.type != "pdf"){
                                if(img){
                                    return '<a href="'+imgurl+'/'+o.image+'" target="_blank" ><img src="'+imgurl+'/'+o.image+'" class="displayImage" style="width: 120px; height: 100px;" ></a>';
                                }else{
                                    return 'No Image';
                                }
                            }else{
                                return '<a href="{{url('freebanner/')}}'+'/'+o.image+'" target="_blank" ><img src="{{asset('/img/pdf-image.jpg')}}" class="displayImage" style="width: 120px; height: 100px;"></a>';
                            }
							
                        }
			        }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == 'inactive')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-edit' aria-hidden='true'></i> Active</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
							               
                            var feature = o.is_feature;

                            if(o.is_feature == 1){
                                return '<button class="btn btn-success">Yes</button>' 
                            }else{
                                return '';
                            }
							
                        }
			        }, 

                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-edit' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);'><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                                      
                            return e+d;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('freebannerData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/freebanner/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Banner ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
