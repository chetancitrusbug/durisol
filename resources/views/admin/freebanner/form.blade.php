<div class="row">
   <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
                {!! Form::label('title', 'Title *', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control','required']) !!} 
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
@if(isset($freebanner) )
    @if($freebanner->feature_image)
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('image',  'View Feature Image', ['class' => 'col-md-4 control-label']) !!}
                    <a href="{{url('/freebanner/'.$freebanner->feature_image)}}" target="_blank"><img src="{{url('freebanner')}}/{!! $freebanner->feature_image !!}" alt="Banner" width="100px" height="100px" class="viewImage"></a>
                </div>
            </div>
        </div>
    
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
                    {!! Form::label('feature_image',  'Change Feature Image', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <input type="file" name="feature_image" id="feature_image" accept="image/*" />
                        {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>   

    @endif
@else
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
                {!! Form::label('feature_image', 'Feature Image', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <input type="file" name="feature_image"  accept="image/*" >
                    {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>   
@endif


    @if(isset($freebanner) )
        @if($freebanner->image)
            <div class="col-sm-12 floatLeft">
                <div class="col-sm-6">
                    <div class="form-group">
                        @if($freebanner->type != "pdf")
                        {!! Form::label('image',  'View File', ['class' => 'col-md-4 control-label']) !!}
                        <a href="{{url('/freebanner/'.$freebanner->image)}}" target="_blank"><img src="{{url('freebanner/thumb/')}}/{!! $freebanner->image !!}" alt="Banner" width="100px" height="100px" class="viewImage"></a>
                        @else
                        {!! Form::label('image', 'View PDF', ['class' => 'col-md-4 control-label']) !!}
                        <a href="{{url('/freebanner/'.$freebanner->image)}}" target="_blank"><img src="{{asset('img/pdf-image.jpg')}}" alt="Banner" width="100px" height="100px" class="viewImage"></a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-12 floatLeft">
                <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                        {!! Form::label('image',  'Change File (Image or PDF) ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <input type="file" name="image" id="image" accept="image/*,application/pdf" />
                            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>   

        @endif
    @else
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image', 'Image or PDF file*', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <input type="file" name="image" id="image" accept="image/*,application/pdf">
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>   
    @endif

    <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                    {!! Form::label('title', 'Description *', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::textarea('description', null, ['class' => 'form-control','required']) !!} 
                        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div> 
        </div>

        <div class="col-sm-12 floatLeft">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('title', 'Make it Featured', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::checkbox('is_feature',null, isset($freebanner) && $freebanner->is_feature == 1 ? true : false  ) !!} 
                        </div>
                    </div>
                </div> 
            </div>

    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">  
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
    </div> 
</div>                   