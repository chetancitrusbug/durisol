@extends('layouts.master2')
@section('title','Testimonial')
@section('content')

@if($testimonial)

 <section class="section section-md text-left bg-white">
        <div class="shell">
          <div class="range">
            @foreach($testimonial as $test)
                <div class="cell-sm-6 cell-md-5 cell-lg-6">
                    <h5>{{$test->business_name}}</h5>
                    <div class="quote-default"> 
                        <div class="quote">
                        <p>
                            <q>{{$test->testimonial_text}}</q>
                        </p>
                        <div class="divider"> </div>
                        <p>
                            <cite class="heading-5">{{$test->person_name}}<span>{{$test->date}}</span></cite>
                        </p>
                        </div>
                    </div>
                </div>
            
            @endforeach
          </div>
        </div>
      </section>
    
@endif


        
@endsection 
