@extends('layouts.master2')

@section('title','Free Download Images')

@section('content')
{{-- 
<!--//================Header end==============//--> 
        <div class="page-header portfolio-four padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li>Freedownload</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================Portfolio start==============//-->
        <div class="padT100 padB100">          
            <div class="mix-default container" >
                <div class="row">
                    <div id="mixItUp">
                      @if(count($freebanner))
                        @foreach($freebanner as $freebanners)
                         <div class="work-gallery simple  col-md-4 col-sm-4 col-xs-12 mix">
                            <div class="theme-hover download_gallery">
                                <figure>
                               
                                   <img src="{{ asset('freebanner/thumb/'.$freebanners->image)}}" alt="{{$freebanners->title}}"/>
                                    <figcaption>
                                        <div class="content">
                                            <div class="content-box">
                                                <a href="#" id="freedownload" data-banners="{{$freebanners->id}}"><i class="fa fa-file-image-o" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        @endforeach
                    @else
                   <p class="no_category">No Images Found</p>
                    @endif
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="pagination">
                    <ul>
                        <li>{!! $freebanner->links() !!}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--//================Portfolio end==============//--> --}}

        <section class="breadcrumb-wrapper breadcrumb-dark">
                <div class="shell">
                  <ol class="breadcrumb-custom">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="#">Free Download Image</a></li>
                    <li>Images
                    </li>
                  </ol>
                </div>
              </section>
             
              {{-- <section class="bg-gray-dark-2">
                <div class="range range-condensed">  
                @if(count($freebanner) > 0 )
                    @foreach($freebanner as $freebanners)      
                  <div class="cell-xs-6 cell-md-4">
                    <div class="box-pricing">
                      <div class="img-block">
                        @if($freebanners->type == 'pdf')
                            <img src="{{ asset('img/pdf-image.jpg')}}" width="640" height="588" alt="{{$freebanners->title}}">
                        @else
                          <img src="{{ asset('freebanner/'.$freebanners->image)}}" width="640" height="588" alt="{{$freebanners->title}}">
                        @endif
                        <h2>{{$freebanners->title}}</h2>
                      </div>
                      <div class="caption">
                        <p>{{$freebanners->description}}</p>
                        <a class="button button-primary" id="freedownload" data-banners="{{$freebanners->id}}" href="#">Download</a>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                 <p class="no_category">No Images Found</p>
                  @endif
                </div>
              </section> --}}
              
             <!-- Hover Row Table-->
              <section class="section-md bg-gray-dark-2">
                <div class="shell">
               
                  <div class="range range-xs-center">
                    <div class="cell-sm-12 cell-lg-9">
                      <div class="table-custom-wrap">
                            @if(count($freebanner)> 0 )
                            @foreach($freebanner as $freebanners)
                             <div class="work-gallery simple  col-md-4 col-sm-4 col-xs-12 mix">
                                <div class="theme-hover download_gallery">
                                    
                                    <figure>
                                    @if($freebanners->feature_image)
                                    <img src="{{ asset('freebanner/'.$freebanners->feature_image)}}" alt="{{$freebanners->title}}"/>

                                    @else

                                        @if($freebanners->type != 'pdf')
                                        <img src="{{ asset('freebanner/thumb/'.$freebanners->image)}}" alt="{{$freebanners->title}}"/>
                                        @else
                                            <img src="{{ asset('img/pdf-image.jpg')}}" alt="{{$freebanners->title}}"/>
                                        @endif
                                    @endif
                                       
                                        
                                        <figcaption>
                                            <div class="content">
                                                <div class="content-box">
                                                    <a href="#" id="freedownload" data-banners="{{$freebanners->id}}"><i class="fa fa-download" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                    
                                </div>
                            </div>
                            @endforeach
                        @else
                       <p class="no_category">No Images Found</p>
                        @endif

                      </div>
                    </div>
                  </div>
                </div>
              </section> 
@endsection

