@extends('layouts.master2')
@section('title','About us')
@section('content')
<section class="breadcrumb-wrapper breadcrumb-dark">
    <div class="shell">
      <ol class="breadcrumb-custom">
        <li><a href="{{url('/')}}">Home</a></li>
        <li>About
        </li>
      </ol>
    </div>
  </section>
  <section class="section section-two-column bg-secondary-2">
    <div class="block-left section-lg context-dark">
      <div class="box-content text-left">
        <div class="shell">
          <div class="range range-center range-md-left">
            <div class="cell-xs-10">
              <h3>WHY DURISOL</h3>
              <p>If you are seeking a build method which is energy efficient, simple to use, fast to construct, and cost effective then British manufactured Durisol insulated concrete form (ICF) units are the solution you are looking for.</p>
      <p>Durisol wall form units are made of wood waste mixed with a cement solution. This results in a form of fossilised wood, as strong as conventional blockwork but with the thermal efficiency of timber. Polyurethane insulation incorporated within each wall form unit further enhances the thermal performance.</p>
      <p>The hollow units are dry stacked together on site to form walls. Since they simply lock together, assembling the units is quick and easy. Once assembled the units are filled with ready-mix concrete. A low slump concrete mix can be poured in temperatures that would halt conventional brick and block laying. When the concrete has set the wall form provides exceptional levels of insulation and airtightness.</p>
      <p>Durisol can save you up to 30% compared to the cost of traditional brick and block masonry.</p>
      <p>Building with Durisol offers a range of unique benefits and meets all upcoming and future regulations and recommendations for sustainable building construction. Click here to find out more about the specific benefits.</p>
              <div class="soc-block"><a class="icon icon-circle icon-circle-md icon-circle-white fa-twitter" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-white fa-facebook-square" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-white fa-instagram" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-white fa-pinterest" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-white fa-youtube-play" href="#"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-right">
      <div class="img-fix"><img src="{{asset('master/images/about-1-960x874.jpg')}}" width="960" height="874" alt=""></div>
    </div>
  </section>
  <section class="section section-lg bg-primary context-dark">
    <div class="shell">
      <h3>Durisol Benefits</h3>
      <div class="range range-40 range-center">
        <div class="cell-xs-6 cell-md-3">
          <div class="box-icon"><span class="icon fa-recycle fa-3x"> </span>
            <h5>Recycled</h5>
            <p class="text">Durisol wall form units are made of wood waste mixed with a cement solution. This results in a form of fossilised wood, as strong as conventional blockwork but with the thermal efficiency of timber.</p>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3">
          <div class="box-icon"><span class="icon mdi mdi-leaf mdi-48px"></span>
            <h5>Light Weight</h5>
            <p class="text">The hollow units are dry stacked together on site to form walls. Since they simply lock together, assembling the units is quick and easy. Once assembled the units are filled with ready-mix concrete.</p>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3">
          <div class="box-icon"><span class="icon fa-money fa-3x"></span>
            <h5>Cost Savings</h5>
            <p class="text">Durisol can save you up to 30% compared to the cost of traditional brick and block masonry.</p>
          </div>
        </div>
        <div class="cell-xs-6 cell-md-3">
          <div class="box-icon"><span class="icon mdi mdi-arrow-all mdi-48px"> </span>
            <h5>Future Improvement</h5>
            <p class="text">Building with Durisol offers a range of unique benefits and meets all upcoming and future regulations and recommendations for sustainable building construction.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-lg bg-gray-dark context-dark"> 
    <div class="shell">
      <h3>Schedule an Appointment</h3>
      <div class="range range-center"> 
        <div class="cell-sm-10 cell-md-8 cell-lg-7"> 
          <p class="text-center text-gray">Your email address will not be published. Required fields are marked with an asterisk symbol ( * )</p>
          {!! Form::open(['url' => '/contact', 'class' => 'text-left form-type-1 form-center','id'=>'formContact','enctype'=>'multipart/form-data' ]) !!}
          @include ('contact.form')
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </section>

        
@endsection 
