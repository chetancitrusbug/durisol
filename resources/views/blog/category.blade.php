{{--  @extends('layouts.master')

@section('title','Blogs')

@section('content')

 <!--//================Header end==============//--> 
        <div class="page-header blog-one padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <h1>Our Blog</h1>
                            <h3><span class="heading-shape">our <strong>blog</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================Portfolio start==============//-->
        <section class="padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="blog-section">
                    @if(count($blogs))
                         @foreach($blogs as $blog)
                        <div class="col-md-4 col-sm-6 floatright col-xs-12">
                            <div class="blog marB30">
                                <figure>
                                    @if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='' )
                                        <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
                                    @else
                                        <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
                                    @endif
                                    <figcaption>
                                         <?php //$created_date= $blog->created_at->format('F, j Y'); ?>
                                           <a class="user-name" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>{{ $blogcategory->title }}</a>
                                        <div class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blog->created_at->diffForHumans() }}</div>
                                    </figcaption>
                                </figure>
                                <div class="blog-caption">
                                    <h4><a href="{!!url("blog/view/".$blog->slug)!!}">{{$blog->title}}</a></h4>
                                   <div class="blog-shortdesc">
										{!! \Illuminate\Support\Str::words($blog->shortdesc, 10) !!}
									</div>
                                    <a href="{!!url("blog/view/".$blog->slug)!!}" class="itg-button light">read more</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
						<p class="no_category">Blog is not found</p>
					@endif
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="pagination">
                    <ul>
                        <li>{!! $blogs->links() !!}</li>
                    </ul>
                </div>
            </div>
        </section>
        <!--//================Portfolio end==============//-->

@endsection  --}}
