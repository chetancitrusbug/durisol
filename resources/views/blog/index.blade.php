@extends('layouts.master2')

@section('title','Blogs')

@section('content')

{{--
 <div class="page-header blog-one padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <h1>our blog</h1>
                            <h3><span class="heading-shape">our <strong>blog</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
 <!--//================Portfolio start==============//-->
        <section class="padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="blog-section with-sidebar">
                            @foreach($blogs as $blog)
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="blog marB30">
                                        <figure>
                                          	@if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='')
                                                <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
                                            @else
                                                <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
                                            @endif
                                            <figcaption>
                                                <div class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blog->created_at->diffForHumans() }}</div>
                                            </figcaption>
                                        </figure>
                                        <div class="blog-caption">
                                            <h4><a href="{!!url("blog/view/".$blog->slug)!!}">{{$blog->title}}</a></h4>
                                           <div class="blog-shortdesc">
                                                 {!! \Illuminate\Support\Str::words($blog->shortdesc, 10) !!} 
                                                 </div>
                                                   <!-- {!! str_limit($blog->shortdesc,20) !!} -->
                                               
                                            <a href="{!!url("blog/view/".$blog->slug)!!}" class="itg-button light">read more</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                                
                               
                                <div class="col-xs-12">
                                    <div class="pagination">
                                        <ul class="pull-left">
                                            <li>{!! $blogs->links() !!}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12">
                        <div class="row">
                            <div class="blog-sidebar">
                                <div class="col-xs-12 widget">
                                    <h3 class="widget_title">Categories</h3>
                                    <ul class="sidebar-category">
                                        @foreach($blogcategory as $catgoryname)
                                            <li><a href="{!!url("blog/category/".$catgoryname->slug)!!}">{{$catgoryname->title}}<span>({{count($catgoryname->blog)}})</span></a></li>
                                        @endforeach
                                    </ul>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
--}}

<section class="breadcrumb-wrapper breadcrumb-dark">
    <div class="shell">
      <ol class="breadcrumb-custom">
        <li><a href="{{url('/')}}">Home</a></li>
        <li>Blog
        </li>
      </ol>
    </div>
  </section>
  <section class="bg-gray-dark-2 section section-sm">
    <div class="shell">
      <div class="range range-60">
        <div class="cell-md-8">
        @foreach($blogs as $blog)
          <div class="post-type-3">
            <div class="img-block">
              <a href="{!!url("blog/view/".$blog->slug)!!}">
                @if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='')
                  <img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}" width="930" height="436" />
                @else
                  <img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}" width="930" height="436" />
                @endif
     
              </a>
            </div>
            <div class="caption">
              <ul class="list-inline meta-list">
                {{-- <li><span>By </span><a href="#">Admin</a></li> --}}
                <li><span class="icon fl-outicons-clock169"></span>{{ $blog->created_at->format('d-m-Y') }}</li>
                {{-- <li><span class="icon fl-outicons-speech-balloon2"></span><a href="#">3 comments</a></li> --}}
              </ul>
              <h5><a href="{!!url("blog/view/".$blog->slug)!!}">{{$blog->title}}</a></h5>
              <p>{!! \Illuminate\Support\Str::words($blog->shortdesc, 50) !!} </p>
              {{-- <ul class="list-inline tag-list">
                <li><span class="icon fl-outicons-tags"></span></li>
                <li><a href="single-post.html">Design</a></li>
                <li><a href="single-post.html">Remodeling</a></li>
                <li><a href="single-post.html">Exterior</a></li>
              </ul> --}}
            </div>
          </div>
        @endforeach
        
          {{-- <div class="range">
            <div class="cell-xs-12">
              <div class="box-pagination">
                <ul class="pagination-custom"> 
                  <li>{!! $blogs->appends(['search' => Request::get('search')])->render() !!}</li>
                   <-- <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li class="disabled active"><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li> . . .</li>
                  <li><a href="#">42</a></li>
                  <li><a class="button button-primary" href="#">Next page<span class="icon fa-angle-double-right"></span></a></li> -->
                </ul>
              </div>
            </div>
          </div> --}}
        </div>
        <div class="cell-md-4">
          <div class="range range-center range-60 blog-sidebar">
            <div class="cell-sm-6 cell-md-12">
              <!-- RD Search-->
              <div class="form-type-1"> 
                <form class="rd-search" action="search-results.html" method="GET" data-search-live="rd-search-results-live">
                  <div class="rd-mailform-inline"> 
                    <div class="form-wrap">
                      <label class="form-label" for="rd-search-form-input">I'm looking for...</label>
                      <input class="form-input" id="rd-search-form-input" type="text" name="s" autocomplete="off">
                    </div>
                    <button class="button button-primary" type="submit">search</button>
                  </div>
                </form>
              </div>
              {{-- <div class="about-box"><img class="img-circle" src="{{asset('master/images/blog-5-450x450.jpg')}}" width="450" height="450" alt="">
                <p>Quoteo Exterior Design company is a boutique professional design team based in Elmira and serving Waterloo, Kitchener, Guelph, and all neighboring local areas...</p>
                <p class="text-white">The team at Quoteo has been providing some top-notch exterior solutions for both commercial and residential clients, with its history spanning over 12 years.</p>
              </div> --}}
            </div>
            {{-- <div class="cell-sm-6 cell-md-12">
              <div class="box-blog-contact bg-gray-dark">
                <h5>Newsletter Subscription</h5>
                <p>Sign up and keep yourself up-to-date with all the latest trends in the world of exterior design!</p>
                <!-- RD Mailform-->
                <form class="rd-mailform text-left form-type-2" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                  <div class="form-wrap">
                    <label class="form-label" for="contact-email">Your Email</label>
                    <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                    <button type="submit"><span class="icon fl-outicons-mail2"></span></button>
                  </div>
                </form>
              </div>
              <div class="box-blog-contact bg-primary context-dark">
                <h5>Follow Us</h5>
                <p>Our exterior designers are posting exterior design news and ideas all the time!</p>
                <div class="soc-block"><a class="icon icon-circle icon-circle-md icon-circle-gray fa-twitter" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-facebook-square" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-instagram" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-pinterest" href="#"></a><a class="icon icon-circle icon-circle-md icon-circle-gray fa-youtube-play" href="#"></a></div>
              </div>
            </div>
            <div class="cell-sm-6 cell-md-12">
              <h5 class="blog-aside-title"> customer quotes</h5>
              <div class="unit unit-vertical unit-xs-horizontal quote-blog unit-md-vertical unit-lg-horizontal">
                <div class="unit__left"><img class="img-circle" src="{{asset('master/images/blog-6-132x132.jpg')}}" width="132" height="132" alt=""></div>
                <div class="unit__body">
                  <p class="big">I was very happy with the work this company did on my house. They were very professional and reliable.</p>
                  <h5 class="text-white">William Barnes</h5>
                  <p class="small text-uppercase text-primary">Freelance Programmer</p>
                </div>
              </div>
            </div> --}}
            {{-- <div class="cell-sm-6 cell-md-12"><a href="#"><img src="{{asset('master/images/banner.jpg')}}" alt=""></a></div> --}}
            <div class="cell-sm-6 cell-md-12">
                @if(count($recentBlogs) > 0)
              <h5 class="blog-aside-title">Recent Blogs</h5>
            
              @foreach($recentBlogs as $blog)
              <div class="post-type-mini">
                <div class="unit unit-vertical unit-xs-horizontal unit-md-vertical unit-lg-horizontal">
                  <div class="unit__left">
                      @if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='')
                        <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}" width="122" height="122"/></a>
                      @else
                        <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}" width="122" height="122"/></a>
                      @endif
                  </div>
                  <div class="unit__body">
                    <div class="post-time"><span class="icon fl-outicons-clock169"></span>{{ $blog->created_at->format('d-m-Y') }}</div>
                    <div class="post-title"><a href="{!!url("blog/view/".$blog->slug)!!}">{{ $blog->title }}</a></div>
                    <p> {!! \Illuminate\Support\Str::words($blog->shortdesc, 15) !!} </p>
                  </div>
                </div>
              </div>
              @endforeach
              @endif

            </div>
            {{-- <div class="cell-sm-6 cell-md-12">
              <h5 class="blog-aside-title">Recent Comments</h5>
              <ul class="list">
                <li><a href="#">Taylor <span class="text-gray">on</span> What's a Hardscape, You Ask? Here's an Overview</a></li>
                <li><a href="#">Derek  <span class="text-gray">on</span> 10 Expertly-Crafted Paint Schemes For Your Home Exterior</a></li>
                <li><a href="#">Mary <span class="text-gray">on</span> Feng Shui Tips for Choosing House Exterior Color</a></li>
                <li><a href="#">Emily <span class="text-gray">on</span> Exterior Paint: Satin or Flat Finish?</a></li>
                <li><a href="#">Alvarez  <span class="text-gray">on</span> List of Exterior Wall Materials Using In Building Construction</a></li>
              </ul>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection  
