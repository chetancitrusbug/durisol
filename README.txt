ALTER TABLE `imagecounter` ADD `training_img_counter` INT NULL DEFAULT NULL AFTER `updated_at`, ADD `training_cat_id` INT NULL DEFAULT NULL AFTER `training_img_counter`;

INSERT INTO `imagecounter` (`id`, `img_counter`, `blog_img_counter`, `blog_cat_id`, `portfolio_cat_id`, `banner_counter`, `freebanner_counter`, `created_at`, `updated_at`,`training_img_counter`,`training_cat_id`) VALUES ('1', '0', '0', '0', '0', '0', '0', NULL, NULL,'0','0');

ALTER TABLE `freebanners` ADD `type` VARCHAR(191) NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `banner` ADD `category_id` INT NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `freebanners` ADD `description` TEXT NULL DEFAULT NULL AFTER `type`;

ALTER TABLE `freebanners` ADD `is_feature` INT(191) NULL DEFAULT '0' AFTER `description`;

ALTER TABLE `freebanners` ADD `feature_image` VARCHAR(191) NULL DEFAULT NULL AFTER `is_feature`;

===============================================================

