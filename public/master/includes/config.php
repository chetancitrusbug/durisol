<?php
setlocale(LC_TIME, "");
setlocale(LC_TIME, "en_AU.utf8"); 
ob_start();
session_start();

//upload folder
define('UPLOADFOLDER','../uploads/');
define('UPLOADFOLDERP','/uploads/');
  
//database credentials
define('DBHOST','localhost');
define('DBUSER','c1fmessential');
define('DBPASS','UDLN5N89D9pbTMsK');
define('DBNAME','c1fmessential');

$db = new PDO("mysql:host=".DBHOST.";dbname=".DBNAME, DBUSER, DBPASS);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//set timezone
date_default_timezone_set('Europe/Zurich');

//load classes as needed
function __autoload($class) {
   $class = strtolower($class);
   $classpath = 'class.'.$class . '.php';
   require_once $classpath;
}

$user = new User($db); 

include('functions.php');
?>