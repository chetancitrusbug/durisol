<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TrainingCategory extends Model
{
    //

    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training_category';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title','slug','status','deleted_at'];

    public function training(){
        return $this->hasMany('App\Training','category_id','id')->where('status','active');
    }
}
