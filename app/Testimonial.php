<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    //

    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'testimonial';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','business_name','testimonial_text','person_name','date','image','deleted_at'];
}
