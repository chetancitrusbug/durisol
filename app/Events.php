<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    //

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title','description','event_date','event_address_1','event_address_2','city','state','feature_image'];
}
