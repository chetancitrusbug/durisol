<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Category;
use App\Setting;
use App\Freebanner;

use App\Bannercategory;
use App\Blog;
use App\Testimonial;


class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::where('status','active')->orderBy('sort_no','asc')->get();
      
        // $portfolio =  Portfolio::where('status','active')->where('featured_portfolio','1')
        // ->with('CategoryName')->get();
         
        // $portfoliocategory =  Bannercategory::where('status','active')->with('getportfolio')->get();
        // foreach( $portfoliocategory as $pcat){
        //         $portfoliolistbycat[]=$pcat->getportfolio->take(5);
        // }
        $blog = Blog::where('status','active')->latest()->limit(3)->get();
       
        $setting = Setting::where('id','1')->first();

        $freebanners = Freebanner::where('type','!=' ,'pdf')->where('is_feature',1)->latest()->limit(3)->get();
    
        
        return view('home',compact('banners','portfolio','portfoliocategory','blog','setting','portfoliolistbycat','freebanners'));
    }

    public function subDomain($account)
    {
        return $account;
    }

    public function redirect()
    {
        return redirect('/admin/users');
    }

    public function aboutindex()
    {
        return view('about');
    }
    public function pricingindex()
    {
        return view('pricing');
    }
    public function privacyindex()
    {
        return view('privacy-policy');
    }
    public function technicalindex()
    {
        return view('technical-info');
    }

    public function testimonialindex()
    {   
        $testimonial = Testimonial::all();
        return view('testimonial',compact('testimonial'));
    }
}
