<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;
use View;
use App\Setting;
class EmailController extends Controller
{
     public function sendMailContactAction($contact) {
        //dd($contact);
        $users= Setting::first();  		
            $subject = "Contact us Inquiry";			
            $to='Admin';
            Mail::send('emails.contactmail', compact('user','to','contact'), function ($message) use ($users,$subject) {
                $message->to($users->email)->subject($subject);
            });           
        
    }
	
	public function sendMailuserImageAction($userlog) {	
            $subject = "Download free Image";			
            $to='Admin';
			$userlogs=$userlog;
			//dd($userlogs->email);
            Mail::send('emails.userimagemail', compact('to','userlog'), function ($message) use ($userlogs,$subject) {
               
                $message->to($userlogs->email)->subject($subject);
            });           
        
    }
}
