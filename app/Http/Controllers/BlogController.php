<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogImages;
use App\Blogcategory;
use DB;

class BlogController extends Controller
{
     public function index()
    {
        $blogcategory = Blogcategory::with('blog')->where('status', '=', 'active')->get();
        $blogs = Blog::where('status', 'active')->latest()->paginate(4);

        $recentBlogs = Blog::where('status', 'active')->limit(3)->latest()->get();
        
    
        return view('blog.index', compact('blogs','blogcategory','recentBlogs'));
    }
    /**
     * 
     * @param type $slug
     * @return type
     */
    public function view($slug)
    {
        $blogcategory = Blogcategory::with('blog')->where('status', '=', 'active')->get();
        $blog = Blog::where('status', '=', 'active')->with('categoryName')->where('slug', '=', $slug)->firstOrFail();
       
        $recentBlogs = Blog::where('status', 'active')
                            ->where('category_id',$blog->category_id)
                            ->where('slug','!=',$slug)
                            ->limit(3)->latest()->get();

        // get previous 
        $previous = Blog::where('id', '<', $blog->id)->latest()->first();
    
        // get next 
        $next = Blog::where('id', '>', $blog->id)->oldest()->first();
       
        $bloggallery = BlogImages::with('blog')->where('blog_id', '=', $blog->id)->get();

        return view('blog.view', compact('blog','bloggallery','recentBlogs','blogcategory','previous','next'));
    }
    /**
     * 
     * @param type $category
     * @return type
     */
    public function serchbycategory($category)
    {
         $blogcategory = Blogcategory::where('status', '=', 'active')->where('slug','=',$category)->first();
         $blogs = Blog::where('status', 'active')->where('category_id','=',$blogcategory->id)->latest()->paginate(3);
        return view('blog.category', compact('blogs','blogcategory'));
    }


}
