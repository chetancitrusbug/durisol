<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Training;
use App\Trainingcategory;
use DB;
use Session;
use App\TrainingImages;
use Yajra\Datatables\Datatables;
use Image;
use File;
use App\Imagecounter;

class TrainingController extends Controller
{
    public function convertVimeo($url)
    {

        if(preg_match(
                '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/',
                $url,
                $matches
            ))
            {
        $id = $matches[2];  

        return 'http://player.vimeo.com/video/'.$id;
            }

    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request) {

        return view('admin.training.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
       
       $training=Training::with('categoryName');

       if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(training.title LIKE '%$value%' OR training.status LIKE '%$value%')";
                $training = Training::with('categoryName')->whereRaw($where_filter);
            }
        }
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $training = $training->where('training.status', $status);
            
        } 
           
        return Datatables::of($training)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
       $trainingCategory = Trainingcategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
        return view('admin.training.create',compact('trainingCategory'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        
		$this->validate($request, [
            'title' => 'required|unique:training',
            //'video_type' => 'required',
			 //'video_url' => 'required', 
            //'video_url' => ['required', 'regex:/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/'],
           // 'page_title' => 'required',
            'category_id' => 'required',
           // 'meta_keyword' => 'required',
           // 'og_title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
           // 'mimage' => 'required',
            'mimage.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'shortdesc' => 'required',
            'longdescription' => 'required',
           // 'page_description' => 'required',
            //'og_description' => 'required',
        ]);

		$data = $request->all();
        if($request->video_type=='youtube'){
			$search     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
            if (preg_match($search, $request->video_url, $matches)) {
			$youtube_id = $matches[count($matches) - 1];
			}	
            $video_url = 'https://www.youtube.com/embed/' . $youtube_id ;
			$data['video_url']= $video_url;
        }else{
            $url = $request->video_url;
            $video_url = $this->convertVimeo($url);
            $data['video_url'] = $video_url;
   
        }
       
        $data['slug'] = str_slug($data['title']);
        $data['status'] = 'active';
        
        if ($request->hasFile('image')) {
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/Training';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);
            $categoryName=Trainingcategory::where('id','=',$request->input('category_id'))->first();
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($categoryName->title);
            }

            $checkimgcounter=Imagecounter::where('training_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->training_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->training_img_counter=1;
                        $imgcounter->training_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $data['image'] = $imagename;
        }

        $training = Training::create($data);
         $files = $request->file('mimage');
        if ($request->hasFile('mimage')) {
            //$image = $request->file('image');
            $i=1;
            foreach ($files as $image) {
                $img = Image::make($image->getRealPath());
                
                $destinationPath = public_path() . '/Training';
                File::exists($destinationPath) or File::makeDirectory($destinationPath);

                $path = $destinationPath . "/thumb";
                File::exists($path) or File::makeDirectory($path);
                
               $categoryName=Trainingcategory::where('id','=',$request->input('category_id'))->first();
                if($request->input('title')){
                    $img_slug = str_slug($request->input('title'));
                }else{
                    $img_slug = str_slug($categoryName->title);
                }
                
               $checkimgcounter=Imagecounter::where('training_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->training_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->training_img_counter=1;
                        $imgcounter->training_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }


                // save original image
                $img->save($destinationPath . '/' . $imagename);

                // save thumbnail image
                $img->fit(480, 270, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/thumb/' . $imagename);
                $dataimage['image']=$imagename;
                $dataimage['training_id'] = $training->id; 
                $image = TrainingImages::create($dataimage);
               
            }
        }

        
        
       Session::flash('flash_message', 'Training added!');
        return redirect('admin/training');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {  
        $training = Training::where('id', $id)->first();
        
        if($training == NULL) {
            Session::flash('flash_error', 'Training is not exist!');
            return redirect('admin/training');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $training->status= 'inactive';
                $training->update();  
                return redirect()->back();
            }else{
                $training->status= 'active';
                $training->update();               
                return redirect()->back();
            }
        } 

        $training['longdescription']=strip_tags($training->longdescription);
        $training['shortdesc']=strip_tags($training->shortdesc);
        $training['og_description']=strip_tags($training->og_description);
        $training['page_description']=strip_tags($training->page_description);
        if($training)
        {
            $images = TrainingImages::where('training_id',$id)->get();
            return view('admin.training.show', compact('training','images'));
            
        }
        else{
            Session::flash('flash_error', 'Training is not exist!');
            return redirect('admin/training');
        }
        
    }
    public function edit(Request $request,$id)
    {
        $training = Training::where('id',$id)->first();
        $trainingCategory = Trainingcategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
        
        if($training)
        {
            $images = TrainingImages::where('training_id',$id)->get();
            return view('admin.training.edit', compact('training','images','trainingCategory'));
        }
        else{
            Session::flash('flash_error', 'Training is not exist!');
            return redirect('admin/training');
        }
    }
    
  
  
 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
     
        $this->validate($request, [
            'title' => 'required',
          //  'video_type' => 'required',
           // 'video_url' => 'required', //'regex:/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/'],
           // 'page_title' => 'required',
            'status' => 'required',
            'category_id' => 'required',
           // 'meta_keyword' => 'required',
            //'og_title' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'mimage' => 'sometimes',
            'mimage.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'shortdesc' => 'required',
            'longdescription' => 'required',
           // 'page_description' => 'required',
            //'og_description' => 'required',
        ]);
        $requestData = $request->all(); 

        if($request->video_type =='youtube'){
             $search     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
            /*$replace    = "youtube.com/embed/$1";    
            $video_url = preg_replace($search,$replace,$request->video_url); */
			if (preg_match($search, $request->video_url, $matches)) {
			$youtube_id = $matches[count($matches) - 1];
			}	
            $video_url = 'https://www.youtube.com/embed/' . $youtube_id ;
			$requestData['video_url']= $video_url;

        }else if($request->video_type =='vimeo'){
             
            $url = $request->video_url;
            $video_url = $this->convertVimeo($url);
            if($video_url){
                 $requestData['video_url'] = $video_url;
            }
            else{
                $requestData['video_url'] = $request->video_url;
            }
            
   
        }else{
             $requestData['video_url'] = '';
        }
        
        $requestData['slug'] = str_slug($requestData['title']);    
        $Training = Training::findOrFail($id);
        if ($request->hasFile('image')) {
           
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/Training';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);
            $categoryName=Trainingcategory::where('id','=',$request->input('category_id'))->first();
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($categoryName->title);
            }

            $checkimgcounter=Imagecounter::where('training_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->training_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->training_img_counter=1;
                        $imgcounter->training_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
            }


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
           $requestData['image'] = $imagename;
        }


         $files = $request->file('mimage');
       
        if ($request->hasFile('mimage')) {
            if($Training->image != '')
            {
                $urlArray = explode('/', $Training->image);
                $imageName = end($urlArray);
                if(file_exists(public_path('Training').'/'.$imageName) AND !empty($imageName)){
                    unlink(public_path('Training').'/'.$imageName);
                }
            }
            //$image = $request->file('image');
            $i=1;
            foreach ($files as $image) {
                $img = Image::make($image->getRealPath());
                
                $destinationPath = public_path() . '/Training';
                File::exists($destinationPath) or File::makeDirectory($destinationPath);

                $path = $destinationPath . "/thumb";
                File::exists($path) or File::makeDirectory($path);
                
               $categoryName=Trainingcategory::where('id','=',$request->input('category_id'))->first();
                if($request->input('title')){
                    $img_slug = str_slug($request->input('title'));
                }else{
                    $img_slug = str_slug($categoryName->title);
                }
                
                $checkimgcounter=Imagecounter::where('training_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->training_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->training_img_counter=1;
                        $imgcounter->training_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->training_img_counter. '.' . $image->getClientOriginalExtension();
                }


                // save original image
                $img->save($destinationPath . '/' . $imagename);

                // save thumbnail image
                $img->fit(480, 270, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/thumb/' . $imagename);
               $imageData['image']=$imagename;
                $imageData['training_id'] = $id;
                 $image = TrainingImages::create($imageData);
               
            }
        }
        
	    $Training->update($requestData);
		Session::flash('flash_message', 'Training Updated !');
		
        return redirect('admin/training');
    }

        
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
       
        $training = Training::find($id);
		if($training->image){
        unlink(public_path('Training') . '/'.$training->image);
		$images = TrainingImages::where('training_id',$id);
        $images->delete();
		}
        $training->delete();
	
        

        if($request->ajax()){
            $message='Deleted';
             return response()->json(['message'=>$message],200);
        }else{
            Session::flash('flash_message','Training Deleted Successfully!');            
            return redirect('admin/Training');
        }
       
    }  


    public function deleteimage(Request $request)
    {
        $id =  $request->id;
        $image = TrainingImages::where('id',$id)->first();
        $image->delete();
        $JSONARRAY = Array(
            'msg'=>'Success',
        );
        // echo json_encode($JSONARRAY);
        exit;
    }     

          

}
