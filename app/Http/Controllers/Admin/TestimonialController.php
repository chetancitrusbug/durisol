<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Testimonial;
use Illuminate\Http\Request;
use Session;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $testimonial = Testimonial::where('business_name', 'LIKE', "%$keyword%")
                ->orWhere('testimonial_text', 'LIKE', "%$keyword%")
                ->orWhere('person_name', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $testimonial = Testimonial::paginate($perPage);
        }

        return view('admin.testimonial.index', compact('testimonial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'business_name' => 'required',
			'testimonial_text' => 'required',
			'person_name' => 'required',
			'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
        $requestData = $request->all();

         if ($request->file('image')) {

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Testimonial'), $filename);
            $requestData['image'] = $filename;

        }
        
        Testimonial::create($requestData);

        Session::flash('flash_message', 'Testimonial added!');

        return redirect('admin/testimonial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        return view('admin.testimonial.show', compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        return view('admin.testimonial.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'business_name' => 'required',
			'testimonial_text' => 'required',
            'person_name' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
			
		]);
        $requestData = $request->all();
        
        $testimonial = Testimonial::findOrFail($id);

         if ($request->file('image')) {

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Testimonial'), $filename);
            $requestData['image'] = $filename;

        }

        $testimonial->update($requestData);

        Session::flash('flash_message', 'Testimonial updated!');

        return redirect('admin/testimonial');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
                

        if($testimonial->image != " " || $testimonial->image != null){
            unlink(public_path('Testimonial/'.$testimonial->image));
        }
        $testimonial->delete();

        Session::flash('flash_message', 'Testimonial deleted!');

        return redirect('admin/testimonial');
    }
}
