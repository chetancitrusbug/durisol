<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use Session;
use Yajra\Datatables\Datatables;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $setting=Setting::where('id','1')->first();
        return view('admin.setting.index',compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
		return view('admin.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {   
       
        
        $this->validate($request, [
            'email' => 'required',
            'contactno' => 'required|numeric|digits_between:10,12'
        ]);
        $data = $request->all();

        if($request->has('twitter')){
            $data['twitter'] = Setting::addhttp($request->twitter);    
        }
        if($request->has('facebook')){
            $data['facebook'] = Setting::addhttp($request->facebook);
        }
        if($request->has('pinterest')){
            $data['pinterest'] = Setting::addhttp($request->pinterest);
        }
        if($request->has('insta')){
            $data['insta'] = Setting::addhttp($request->insta);
        }
    
        $setting = Setting::create($data);
    
        Session::flash('flash_message', 'Setting added!');
        return redirect('admin/setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $setting = Setting::where('id', $id)->first();
       
             return view('admin.setting.edit', compact('setting'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
         $this->validate($request, [
            'email' => 'required',
            'address' => 'required',
            'contactno' => 'required|numeric|digits_between:10,11'
        ]);
        $requestData = $request->all();
        if($request->has('twitter')){
            $requestData['twitter'] = Setting::addhttp($request->twitter);    
        }
        if($request->has('facebook')){
            $requestData['facebook'] = Setting::addhttp($request->facebook);
        }
        if($request->has('pinterest')){
            $requestData['pinterest'] = Setting::addhttp($request->pinterest);
        }
        if($request->has('insta')){
            $requestData['insta'] = Setting::addhttp($request->insta);
        }
       // dd($requestData);

        $setting = Setting::findOrFail($id);
        $setting->update($requestData);

        Session::flash('flash_message', 'Setting Updated Successfully!');
        return redirect('admin/setting');
    }


    
}
