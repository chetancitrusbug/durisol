<?php

namespace App\Http\Controllers\Admin;

use App\Trainingcategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;

class TrainingcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {             
        return view('admin.trainingcategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.trainingcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:training_category',
            'status' => 'required',
        ]);
        $data = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));

        $data['slug'] = str_slug($data['title']); 
        //dd($data);
        $trainingcategory = Trainingcategory::create($data);

        Session::flash('flash_message', 'Training Category added!');
        return redirect('admin/trainingcategory');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $trainingcategory = Trainingcategory::where('id', $id)->first();

        if ($trainingcategory) {
            return view('admin.trainingcategory.edit', compact('trainingcategory'));
        } else {
            Session::flash('flash_error', 'training category is not exist!');
            return redirect('admin/trainingcategory');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'status' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));
        $requestData['slug'] = str_slug($requestData['title']); 
        
        $trainingcategory = Trainingcategory::where('id', $id);
        $trainingcategory->update($requestData);
        Session::flash('flash_message', 'Training Category Updated Successfully!');
        return redirect('admin/trainingcategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {

        $trainingcategory = Trainingcategory::where('id', $id);
        $trainingcategory->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $trainingcategory = Trainingcategory::All();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(training_category.title LIKE  '%$value%' OR training_category.status LIKE  '%$value%'  )";
                $trainingcategory = Trainingcategory::whereRaw($where_filter);
            }
        }
        
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $trainingcategory = $trainingcategory->where('status', $status);
            
        }
        return Datatables::of($trainingcategory)
            ->make(true);
        exit;
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $trainingcategory = Trainingcategory::where('id', $id)->first();
        if($trainingcategory == NULL) {
            Session::flash('flash_error', 'Training Category is not exist!');
            return redirect('admin/trainingcategory');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $trainingcategory->status= 'inactive';
                $trainingcategory->update();            

                return redirect()->back();
            }else{
                $trainingcategory->status= 'active';
                $trainingcategory->update();               
                return redirect()->back();
            }

        }
        return view('admin.trainingcategory.show', compact('trainingcategory'));
    }
}
