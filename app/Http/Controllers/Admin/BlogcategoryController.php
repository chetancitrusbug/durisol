<?php

namespace App\Http\Controllers\Admin;

use App\Blogcategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;

class BlogcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {             
        return view('admin.blogcategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.blogcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:blog_category',
            'status' => 'required',
        ]);
        $data = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));

        $data['slug'] = str_slug($data['title']); 
        //dd($data);
        $blogcategory = Blogcategory::create($data);

        Session::flash('flash_message', 'Blog Category added!');
        return redirect('admin/blogcategory');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $blogcategory = Blogcategory::where('id', $id)->first();

        if ($blogcategory) {
            return view('admin.blogcategory.edit', compact('blogcategory'));
        } else {
            Session::flash('flash_error', 'Blog category is not exist!');
            return redirect('admin/blogcategory');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'status' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));
        $requestData['slug'] = str_slug($requestData['title']); 
        
        $blogcategory = Blogcategory::where('id', $id);
        $blogcategory->update($requestData);
        Session::flash('flash_message', 'Blog Category Updated Successfully!');
        return redirect('admin/blogcategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {

        $blogcategory = Blogcategory::where('id', $id);
        $blogcategory->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $blogcategory = Blogcategory::All();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(blog_category.title LIKE  '%$value%' OR blog_category.status LIKE  '%$value%'  )";
                $blogcategory = Blogcategory::whereRaw($where_filter);
            }
        }
        
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $blogcategory = $blogcategory->where('status', $status);
            
        }
        return Datatables::of($blogcategory)
            ->make(true);
        exit;
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $blogcategory = Blogcategory::where('id', $id)->first();
        if($blogcategory == NULL) {
            Session::flash('flash_error', 'Category is not exist!');
            return redirect('admin/blogcategory');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $blogcategory->status= 'inactive';
                $blogcategory->update();            

                return redirect()->back();
            }else{
                $blogcategory->status= 'active';
                $blogcategory->update();               
                return redirect()->back();
            }

        }
        return view('admin.blogcategory.show', compact('blogcategory'));
    }
}
