<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FreeImageUserLog;
use Yajra\Datatables\Datatables;

class FreeimagedownloaduserlogController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.imageuserlog.index');
    }

     /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $freeimageuserlog = FreeImageUserLog::orderby('id','desc')->select('*');
        //dd($freebanner);
        return Datatables::of($freeimageuserlog)
            ->make(true);
        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {

        $freeimageuserlog = FreeImageUserLog::where('id', $id)->first();
        $freeimageuserlog->delete();
        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }
}
