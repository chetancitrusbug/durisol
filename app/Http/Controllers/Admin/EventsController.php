<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;
use Illuminate\Http\Request;
use Session;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $events = Event::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('event_date', 'LIKE', "%$keyword%")
                ->orWhere('event_address_1', 'LIKE', "%$keyword%")
                ->orWhere('event_address_2', 'LIKE', "%$keyword%")
                ->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('state', 'LIKE', "%$keyword%")
                ->orWhere('feature_image', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $events = Event::paginate($perPage);
        }

        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required',
			'event_address_1' => 'required',
			'city' => 'required',
            'state' => 'required',
            'feature_image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
        $requestData = $request->all();

         if ($request->file('feature_image')) {

            $fimage = $request->file('feature_image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Events'), $filename);
            $requestData['feature_image'] = $filename;

        }
        
        Event::create($requestData);

        Session::flash('flash_message', 'Event added!');

        return redirect('admin/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required',
			'event_address_1' => 'required',
			'city' => 'required',
            'state' => 'required',
            'feature_image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
        $requestData = $request->all();
        
        $event = Event::findOrFail($id);

        if ($request->file('feature_image')) {

            $fimage = $request->file('feature_image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Events'), $filename);
            $requestData['feature_image'] = $filename;

        }

        $event->update($requestData);

        Session::flash('flash_message', 'Event updated!');

        return redirect('admin/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        if($event->feature_image != " "){
            unlink(public_path('Events') . '/'.$event->feature_image);
        }
        $event->delete();

        Session::flash('flash_message', 'Event deleted!');

        return redirect('admin/events');
    }
}
