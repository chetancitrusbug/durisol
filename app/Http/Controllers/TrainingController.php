<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Training;
use App\TrainingImages;
use App\Trainingcategory;
use DB;

class TrainingController extends Controller
{
     public function index()
    {
        $trainingcategory = Trainingcategory::with('training')->where('status', '=', 'active')->get();

        $trainings = Training::where('status', 'active')->latest()->paginate(4);


        $recentTrainings = Training::where('status', 'active')->limit(3)->latest()->get();
        
    
        return view('training.index', compact('trainings','trainingcategory','recentTrainings'));
    }
    /**
     * 
     * @param type $slug
     * @return type
     */
    public function view($slug)
    {
        $trainingcategory = Trainingcategory::with('training')->where('status', '=', 'active')->get();
        $training = Training::where('status', '=', 'active')->with('categoryName')->where('slug', '=', $slug)->firstOrFail();
       
        $recentTrainings = Training::where('status', 'active')
                            ->where('category_id',$training->category_id)
                            ->where('slug','!=',$slug)
                            ->limit(3)->latest()->get();

        // get previous 
        $previous = Training::where('id', '<', $training->id)->latest()->first();
    
        // get next 
        $next = Training::where('id', '>', $training->id)->oldest()->first();
       
        $traininggallery = TrainingImages::with('training')->where('training_id', '=', $training->id)->get();

        return view('training.view', compact('training','traininggallery','recentTrainings','trainingcategory','previous','next'));
    }
    /**
     * 
     * @param type $category
     * @return type
     */
    public function serchbycategory($category)
    {
         $trainingcategory = Trainingcategory::where('status', '=', 'active')->where('slug','=',$category)->first();
         $trainings = Training::where('status', 'active')->where('category_id','=',$trainingcategory->id)->latest()->paginate(3);
        return view('training.category', compact('trainings','trainingcategory'));
    }


}
