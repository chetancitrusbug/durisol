<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Package;
use App\Opportunity;
use DB;

class UsersController extends Controller

{

    protected $model;

    public function __construct()
    {
        $this->model = new User;
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */


    public function login(Request $request)
    {
      
       //echo '<pre>';print_r($request->all());exit;
        $data = [];
        $message = "";
        $validation = [];
        $code = 200;
        $status = true;

        $user = User::where("email", $request->email)->first();
        //echo '<pre>';print_r($user);exit;

        if ($user && $user->active == '0') {

            $message = "You are not activited yet.please check your email for activation.";
            $status = false;

        }

        if ($user && $status) {
        
            if (Hash::check($request->password, $user->password)) {
                $user->api_token = md5(uniqid());
                $user->save();
                $message = __('Login Success');
                $data = $user;
            } else {              
                $message = __('Invalid username or password');
                $code = \Config::get('constants.responce_code.bad_request');
                $status = false;
            }
        } else {         
            if ($message == '') {
                $message = "Invalid username or password";
            }
            $status = false;
        }
        //return $code;
       return $this->toJson($status, $data, $message, $validation, $code);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    protected function register(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;

        $rules = array(
            'name' => 'required',            
            'email' => 'required|email|unique:users',
            'password' => 'required'
        );


        $validator = \Validator::make($request->all(), $rules, []);


        if ($validator->fails()) {

            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $message = __('Not valid data');

        } else {


            $data = $request->except('password');
            $data['name'] = $request->name;
            $data['password'] = bcrypt($request->password);
            $data['api_token'] = md5(uniqid());
            //$data['activation_token'] = sha1(time() . uniqid() . $data['email']);


            $user = User::create($data);
            if ($user) {

               // $user->notify(new  \App\Notifications\ActivationLink($user));
                $data = $user;
                $message = __('Registration Successful. Please check your email for activation link.');
            } else {
                $status = false;
                $code = \Config::get('constants.responce_code.bad_request');
                $message = __('Something went wrong ! Please try again later');
            }

        }
        return $this->toJson($status, $data, $message, $validation, $code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function changePassword(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;

        $id = $request->user_id;

        $rules = array(
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|same:password',
        );


        $messsages = array(
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        );

        $validator = \Validator::make($request->all(), $rules, $messsages);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
        } else {

            $user = User::where("id", $id)->first();


            //if ($user && Hash::check($request->input('current_password'), $user->password)) {
            if ($user) {

                $user->password = Hash::make($request->input('password'));
                $user->save();
                $message = __('Password changed successfully.');

            } else {
                $validator->errors()->add('current_password', __('Please enter correct current password'));
                $validation = $validator;
                $code = \Config::get('constants.responce_code.validation_failed');
                $status = false;
            }

        }

        return $this->toJson($status, $data, $message, $validation, $code);
    }

    public function forgotPasswordPost(Request $request)
    {
       
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });
       
        if ($response == "passwords.sent") {
            $message = __('Password reset link has been sent to email');
        } else if ($response == "passwords.user") {
            $status = false;
            $message = __('User not found');
            $code = 400;
        } else {
            $status = false;
            $message = __('Something went wrong ! Please try again later');
            $code = 400;
        }

        return $this->toJson($status, $data, $message, $validation, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function update(Request $request)
    {
        $id = $request->user_id;
        $result = $this->model->updateUser($id, $request);

        return $this->toJson($result['status'], $result['data'], $result['message'], $result['validation'], $result['code']);


    }
    public function getpackage(Request $request)
    {
        $kw = $request->kw;
        $title = $request->title;
        $code = 200;
        $status = true;
        $data = [];
        $message = "";
        $validation = [];
        if($kw){
            $package = Package::where("kw", $kw)->Where("title",'LIKE', "%$title%")->get();
        }else{
            $package = Package::where("title",'LIKE', "%$title%")->get();
        }
        if($request->all() != NULL){
            if(count($package)>0){
                $message = __('Packages are found');
                $data = $package;
            }else{
                $message = __('No packages available!');
                $status = false;
            }
        }else{
            $package = Package::all();
            $message = __('Packages are found');
            $data = $package;
        }    
        
        return $this->toJson($status, $data, $message, $validation, $code);

    }

    public function logout(Request $request)
    {
        $data = [];
        $message = "";
        $validation = [];
        $code = 200;
        $status = true;

        if ($request->has('api_token')) {
            $api_token = $request->get('api_token');
            User::where("api_token", $api_token)->update(["api_token" => ""]);
        }
        $message = "Logout success!";
        return $this->toJson($status, $data, $message, $validation, $code);
    }

    public function getOpportunity(Request $request)
    {   
        $code = 200;
        $status = true;
        $data = [];
        $message = "";
        $validation = [];

        $id = $request->user_id;
        $user = User::where("id", $id)->first();


        //if ($user && Hash::check($request->input('current_password'), $user->password)) {
        if ($user) {
            $opportunity = Opportunity::where('owner_id',$user->capsule_id)->where('closed',null)->get();
            if(count($opportunity)>0){
                $message = __('Opportunities are found');
                $data = $opportunity;
            }else{
                $message = __('No Opportunities Created!');
                $status = false;
            }
            
        } else {
            $status = false;
            $message = __('User not found');
            $code = 400;
        }
        return $this->toJson($status, $data, $message, $validation, $code);

    }
}
