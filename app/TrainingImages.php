<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingImages extends Model
{
    //

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['training_id', 'image'];

    public function training(){
    return $this->hasOne('App\Training','id','training_id');
}
}
