<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    //
    
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title' ,'slug' , 'shortdesc','longdescription','category_id','image','video_type', 'video_url','page_title','page_description','meta_keyword','og_title','og_description','status','deleted_at'];

    public function categoryName(){
        return $this->hasOne('App\Trainingcategory','id','category_id')->select('training_category.id as id','training_category.title as cat_title');
    }  


}
