<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->longText('shortdesc')->nullable();
            $table->longText('longdescription')->nullable();
			$table->integer('category_id')->nullable();
            $table->string('image')->nullable();
            $table->string('video_type')->nullable();
            $table->string('video_url')->nullable();
            $table->string('page_title')->nullable();
            $table->longText('page_description')->nullable();
            $table->longText('meta_keyword')->nullable();
            $table->string('og_title')->nullable();
            $table->longText('og_description')->nullable(); 
            $table->string('status')->default('active');  
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('blogs');
    }
}
